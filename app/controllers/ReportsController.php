<?php

class ReportsController extends \BaseController {

	public function showAllBlogs(){

		// return "hello";
		
		return Datatable::collection(Area::all())
			->searchColumns('_id','thana','district')
			->orderColumns('_id')
			->addColumn('ID',function($model){
				return $model->id;
			})
			->addColumn('THANA',function($model){
				return $model->thana;
			})
			->addColumn('DISTRICT',function($model){
				return $model->district;
			})
			->make();
	}

	public function ajax(){
		// return "balda";

		$cols = [

			  "agency" ,
			  "date" ,
			  "country" ,
			  "division" ,
			  "district",
			  "thana" ,
			  "union",
			  "village" ,
			  "uthan_owner_name" ,
			  "uthan_owner_mobile",
			  "uthan_participant_name",
			  "uthan_participant_mobile" ,
			  "shop_owner_name",
			  "shop_owner_mobile",
			  "haat_participant_name",
			  "haat_participant_mobile",
			  "college_name",
			  "principal_name",
			  "principal_mobile",
			  "teacher_name",
			  "teacher_mobile",
			  "total_contact",
			  "visit_images1",
			  "visit_images2",
			  "visit_images3",
			  "visit_images4",
			  "visit_certificate1",
			  "visit_certificate2",
			  "visit_certificate3",
			  "visit_certificate4",
			  "visit_date1",
			  "visit_date2",
			  "visit_date3",
			  "visit_date4",
			  "certificate",
			  "edit",
			  "show"

		];	
		
		return DTmongo::ajax('entries',$cols);
	}
}
