<?php

class UsersController extends \BaseController {

	public function __construct(){

		$this->beforeFilter('csrf',['on'=>'post']);

	}

	public function getLogin(){

		if(Auth::check()){

			return Redirect::to('dashboard');
		}
		return View::make('login');
	}



	public function postLoginCheck(){

		$remember = (Input::get('remember'));

		$remember = isset($remember);
		

		if(Auth::attempt(['username'=>Input::get('username'),'password'=>Input::get('password')],$remember)){
			
			return Redirect::to('dashboard');
			
		}else{

			return Redirect::to('users/login');
		}

	}

}