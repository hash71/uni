<?php

class EntriesController extends BaseController {

	public function store(){

		// return dd(Input::all());
		$entry = new Entry;

		$entry->campaign = Auth::user()->campaign;

		$entry->id = time();

		$entry->type = Input::get('type');

		

		$entry->operator = 1;

		$entry->agency_admin = 1;

		$entry->agency = Input::get('agency');
		$entry->date = Input::get('date');
		$entry->country = Input::get('country');
		$entry->division = Input::get('division');
		$entry->district = Input::get('district');
		$entry->thana = Input::get('thana');
		$entry->union = Input::get('union');
		$entry->village = Input::get('village');

		$entry->uthan_owner_name = Input::get('uthan_owner_name')?Input::get('uthan_owner_name'):'';
		$entry->uthan_owner_mobile = Input::get('uthan_owner_mobile')?Input::get('uthan_owner_mobile'):'';
		$entry->uthan_participant_name = Input::get('uthan_participant_name')?Input::get('uthan_participant_name'):'';
		$entry->uthan_participant_mobile = Input::get('uthan_participant_mobile')?Input::get('uthan_participant_mobile'):'';

		$entry->shop_owner_name = Input::get('shop_owner_name')?Input::get('shop_owner_name'):'';
		$entry->shop_owner_mobile = Input::get('shop_owner_mobile')?Input::get('shop_owner_mobile'):'';
		$entry->haat_participant_name = Input::get('haat_participant_name')?Input::get('haat_participant_name'):'';
		$entry->haat_participant_mobile = Input::get('haat_participant_mobile')?Input::get('haat_participant_mobile'):'';

		$entry->college_name = Input::get('college_name')?Input::get('college_name'):'';
		$entry->principal_name = Input::get('principal_name')?Input::get('principal_name'):'';
		$entry->principal_mobile = Input::get('principal_mobile')?Input::get('principal_mobile'):'';
		$entry->teacher_name = Input::get('teacher_name')?Input::get('teacher_name'):'';
		$entry->teacher_mobile = Input::get('teacher_mobile')?Input::get('teacher_mobile'):'';

		$entry->total_contact = Input::get('total_contact');
		
		$entry->certificate = '';

		$entry->certificate_link = '';

		$entry->visit_images1 = '';
		$entry->visit_images_link1 = '';
		$entry->visit_certificate1 = '';
		$entry->visit_certificate_link1 = '';

		$entry->visit_images2 = '';
		$entry->visit_images_link2 = '';
		$entry->visit_certificate2 = '';
		$entry->visit_certificate_link2 = '';

		$entry->visit_images3 = '';
		$entry->visit_images_link3 = '';
		$entry->visit_certificate3 = '';
		$entry->visit_certificate_link3 = '';

		$entry->visit_images4 = '';
		$entry->visit_images_link4 = '';
		$entry->visit_certificate4 = '';
		$entry->visit_certificate_link4 = '';



		//completion certificate

		$file = Input::file('certificate');

		if($file) {
			
			$destinationPath = base_path().'/public/uploads/';
			$filename = time().$file->getClientOriginalName();
			$upload_success = $file->move($destinationPath, $filename);

			if ($upload_success) {
			// resizing an uploaded file
				Image::make($destinationPath . $filename)->resize(800, 600)->save($destinationPath . $filename);

			// return Response::json('success', 200);
			} else {
			// return Response::json('error', 400);
			}

			$ts = (time()+20);
			$link = '<a data-lightbox="'.$ts.'"'. ' href ='.'"'.Request::root().'/uploads/'.$filename.'"'.'>'. 'image'.'</a>' ;
			$entry->certificate = $link;
			$entry->certificate_link = url('/').'/uploads/'.$filename;

		}
		
		//completion certificate end	
		

		

		for($i=1; $i<=4; $i++){

			$a = array();
			$b = array();

			$index = 0;

			$ts = time().$i;
			
			foreach (Input::file('images'.$i) as $image) {        
				
				$index++;
				
				$file = $image;

				if($file) {
					
					$destinationPath = base_path().'/public/uploads/';
					$filename = time().$file->getClientOriginalName();
					$upload_success = $file->move($destinationPath, $filename);

					if ($upload_success) {
				// resizing an uploaded file


						Image::make($destinationPath . $filename)->resize(800, 600)->save($destinationPath . $filename);

					// return Response::json('success', 200);
					} else {
					// return Response::json('error', 400);
					}

					$link = '<a data-lightbox="'.$ts.'"'. ' href ='.'"'.Request::root().'/uploads/'.$filename.'"'.'>'. 'image'.$index  . '</a>' ;
					$link_path = url('/').'/uploads/'.$filename;
					array_push($a, $link);
					array_push($b, $link_path);
				}
				


			}
			$visit = 'visit_images'.$i;
			$visit_link = 'visit_images_link'.$i;
			$entry->$visit = $a;
			$entry->$visit_link = $b;

		}
		
		for($i = 1; $i<=4; $i++){

			$file = Input::file('certificate'.$i);

			if($file) {
				
				$destinationPath = base_path().'/public/uploads/';
				$filename = time().$file->getClientOriginalName();
				$upload_success = $file->move($destinationPath, $filename);

				if ($upload_success) {
			// resizing an uploaded file
					Image::make($destinationPath . $filename)->resize(800, 600)->save($destinationPath . $filename);

				// return Response::json('success', 200);
				} else {
				// return Response::json('error', 400);
				}

				$ts = (time()+10).$i;
				
				$link = '<a data-lightbox="'.$ts.'"'. ' href ='.'"'.Request::root().'/uploads/'.$filename.'"'.'>'. 'image'.$index  . '</a>' ;
				
				$link_path = url('/').'/uploads/'.$filename;
				
				$visit = 'visit_certificate'.$i;
				
				$visit_link = 'visit_certificate_link'.$i;
				
				$entry->$visit = $link;

				$entry->$visit_link = $link_path;

			}
			
		}

		for($i = 1; $i<=4; $i++){

			$time = Input::get('date'.$i);

			$visit = 'visit_date'.$i;

			$entry->$visit = $time;

		}	
		

		// Entry::truncate();
		
		$entry->save();

		$entry->edit = '<a href="'.URL::to('entity',$entry->_id).'">'.'Edit'.'</a>';

		$entry->show = '<a href="'.URL::to('showentity',$entry->_id).'">'.'Show'.'</a>';

		$entry->save();

		return Redirect::to('dataTable');

		
	}

}
