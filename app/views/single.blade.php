@extends('layouts.default.master')
@section('content')
<!-- row -->
<div class="row">
  <!-- col 12 -->
  <div class="col-md-8">
    <!-- tile -->
    <section class="tile color blue">
      <!-- tile header -->
      <div class="tile-header color rounded-top-corners">
        <h1><strong>{{ucfirst($entity->type)}} Details</strong></h1>
        <div class="controls">
          <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
          <a href="#" class="remove"><i class="fa fa-times"></i></a>
        </div>
      </div>
      <!-- /tile header -->
      <!-- tile body -->
      <div class="tile-body">
        <div class="form-details">
          <div class="single-details odd">
            <div class="col-sm-4">
              <label>Agency</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->agency}}</p>
            </div>
          </div>
          <div class="single-details even">
            <div class="col-sm-4">
              <label>Date</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->date}}</p>
            </div>
          </div>
          <div class="single-details odd">
            <div class="col-sm-4">
              <label>Country</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->country}}</p>
            </div>
          </div>
          <div class="single-details even">
            <div class="col-sm-4">
              <label>Division</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->division}}</p>
            </div>
          </div>
          <div class="single-details odd">
            <div class="col-sm-4">
              <label>District</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->district}}</p>
            </div>
          </div>
          <div class="single-details even">
            <div class="col-sm-4">
              <label>Thana</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->thana}}</p>
            </div>
          </div>
          <div class="single-details odd">
            <div class="col-sm-4">
              <label>Union</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->union}}</p>
            </div>
          </div>
          <div class="single-details even">
            <div class="col-sm-4">
              <label>Village</label>
            </div>
            <div class="col-sm-8">
              <p>{{$entity->village}}</p>
            </div>
          </div>

          @if($entity->type == 'uthan')
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Uthan Owner's Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->uthan_owner_name}}</p>
              </div>
            </div>
            <div class="single-details even">
              <div class="col-sm-4">
                <label>Uthan Owner's Cell</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->uthan_owner_mobile}}</p>
              </div>
            </div>
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Uthan Participant's Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->uthan_participant_name}}</p>
              </div>
            </div>
            <div class="single-details even">
              <div class="col-sm-4">
                <label>Uthan Participant's Cell</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->uthan_participant_mobile}}</p>
              </div>
            </div>
          @endif

          @if($entity->type == 'haat')
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Shop Owner's Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->shop_owner_name}}</p>
              </div>
            </div>
            <div class="single-details even">
              <div class="col-sm-4">
                <label>Shop Owner's Cell</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->shop_owner_mobile}}</p>
              </div>
            </div>
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Haat Participant's Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->haat_participant_name}}</p>
              </div>
            </div>
            <div class="single-details even">
              <div class="col-sm-4">
                <label>Haat Participant's Cell</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->haat_participant_mobile}}</p>
              </div>
            </div>

          @endif


          @if($entity->type == 'college')
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>College Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->college_name}}</p>
              </div>
            </div>            
            <div class="single-details even">
              <div class="col-sm-4">
                <label>Principal Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->principal_name}}</p>
              </div>
            </div>
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Principal's Cell</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->principal_mobile}}</p>
              </div>
            </div>
            <div class="single-details even">
              <div class="col-sm-4">
                <label>Teacher Name</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->teacher_name}}</p>
              </div>
            </div>
            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Teacher's Cell</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->teacher_mobile}}</p>
              </div>
            </div>

          @endif

          @if($entity->type == 'college')

            <div class="single-details even">
              <div class="col-sm-4">
                <label>Total Contact</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->total_contact}}</p>
              </div>
            </div> 

          @else

            <div class="single-details odd">
              <div class="col-sm-4">
                <label>Total Contact</label>
              </div>
              <div class="col-sm-8">
                <p>{{$entity->total_contact}}</p>
              </div>
            </div>

          @endif      

        </div>
      </div>
      <!-- /tile body -->
    </section>
    <!-- /tile -->
  </div>
  <!-- /col 9 -->
  <div class="col-md-4">
    <section class="tile color" style="background-color:#F64B1E">
      <!-- tile header -->
      <div class="tile-header text-center">
        <h1><strong>Completion Certificate</strong></h1>
        <hr>
      </div>
      <!-- /tile header -->
      <!-- tile body -->
      <div class="tile-body">
        <div class="certImg">
        
          <?php 
            echo "<a href=".$entity->certificate_link." "."data-lightbox=".'"completion_certificate"'.">";

            echo '<img class="img-responsive" src="'.$entity->certificate_link.'" alt="">'."</a>";
          ?> 
        </div>
      </div>
      <!-- /tile body -->
    </section>
    <section class="tile color" style="background-color:#731627">
      <!-- tile widget -->
      <div class="tile-widget nopadding color transparent-black rounded-top-corners">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#visit1" aria-controls="visit1" role="tab" data-toggle="tab">Visit  1</a></li>
          <li role="presentation"><a href="#visit2" aria-controls="visit2" role="tab" data-toggle="tab">Visit  2</a></li>
          <li role="presentation"><a href="#visit3" aria-controls="visit3" role="tab" data-toggle="tab">Visit  3</a></li>
          <li role="presentation"><a href="#visit4" aria-controls="visit4" role="tab" data-toggle="tab">Visit  4</a></li>
        </ul>
        <!-- / Nav tabs -->
      </div>
      <!-- /tile widget -->
      <!-- tile body -->
      <div class="tile-body rounded-bottom-corners">
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="visit1">
            <h2 class="text-center"><strong>Visit Details</strong></h2>
            <h5 class="text-center"><strong>Visit Date</strong> {{$entity->visit_date1}}</h5>
            <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
            <div class="certImg">
              <?php
              echo "<a href=".$entity->visit_certificate_link1." "."data-lightbox=".'"visit_certificate_link1"'.">";
              echo '<img class="img-responsive" src="'.$entity->visit_certificate_link1.'" alt="">'."</a>";
              ?>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit2">
            <h2 class="text-center"><strong>Visit Details</strong></h2>
            <h5 class="text-center"><strong>Visit Date</strong> {{$entity->visit_date2}}</h5>
            <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
            <div class="certImg">
              <?php
              echo "<a href=".$entity->visit_certificate_link2." "."data-lightbox=".'"visit_certificate_link2"'.">";
              echo '<img class="img-responsive" src="'.$entity->visit_certificate_link2.'" alt="">'."</a>";
              ?>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit3">
            <h2 class="text-center"><strong>Visit Details</strong></h2>
            <h5 class="text-center"><strong>Visit Date</strong> {{$entity->visit_date3}}</h5>
            <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
            <div class="certImg">
              <?php
              echo "<a href=".$entity->visit_certificate_link3." "."data-lightbox=".'"visit_certificate_link3"'.">";
              echo '<img class="img-responsive" src="'.$entity->visit_certificate_link3.'" alt="">'."</a>";
              ?>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit4">
            <h2 class="text-center"><strong>Visit Details</strong></h2>
            <h5 class="text-center"><strong>Visit Date</strong> {{$entity->visit_date4}}</h5>
            <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
            <div class="certImg">
              <?php
              echo "<a href=".$entity->visit_certificate_link4." "."data-lightbox=".'"visit_certificate_link4"'.">";
              echo '<img class="img-responsive" src="'.$entity->visit_certificate_link4.'" alt="">'."</a>";
              ?>
            </div>
          </div>
        </div>
      </div>
      <!-- /tile body -->
    </section>
    <section class="tile color" style="background-color:#A63F82">
      <!-- tile widget -->
      <div class="tile-widget nopadding color transparent-black rounded-top-corners">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#visit1Photos" aria-controls="visit1Photos" role="tab" data-toggle="tab">Visit 1</a></li>
          <li role="presentation"><a href="#visit2Photos" aria-controls="visit2Photos" role="tab" data-toggle="tab">Visit 2</a></li>
          <li role="presentation"><a href="#visit3Photos" aria-controls="visit3Photos" role="tab" data-toggle="tab">Visit 3</a></li>
          <li role="presentation"><a href="#visit4Photos" aria-controls="visit4Photos" role="tab" data-toggle="tab">Visit 4</a></li>
        </ul>
        <!-- / Nav tabs -->
      </div>
      <!-- /tile widget -->
      <!-- tile body -->
      <div class="tile-body rounded-bottom-corners">
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="visit1Photos">
            <h2 class="text-center"><strong>Visit Photos</strong></h2>
            <h4 class="text-center">Visit 1 </h4>
            <div id="owl-example" class="owl-carousel">

              <?php $i=0; ?>

              @foreach($entity->visit_images_link1 as $image)                                  
              
                <div class="text-center">
                  <?php
                    echo "<a href=".$image." "."data-lightbox=".'"visit_images_link1"'.">";
                    echo '<img class="" src="'.$image.'" alt="">'."</a>";
                  ?>
                </div>

                <?php $i++; ?>              

              @endforeach
              
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit2Photos">
            <h2 class="text-center"><strong>Visit Photos</strong></h2>
            <h4 class="text-center">Visit 2 </h4>
             <div id="owl-example" class="owl-carousel">

              <?php $i=0; ?>

              @foreach($entity->visit_images_link2 as $image)                                  
              
                <div class="text-center">
                  <?php
                    echo "<a href=".$image." "."data-lightbox=".'"visit_images_link2"'.">";
                    echo '<img class="" src="'.$image.'" alt="">'."</a>";
                  ?>
                </div>

                <?php $i++; ?>              

              @endforeach
              
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit3Photos">
            <h2 class="text-center"><strong>Visit Photos</strong></h2>
            <h4 class="text-center">Visit 3 </h4>
             <div id="owl-example" class="owl-carousel">

              <?php $i=0; ?>

              @foreach($entity->visit_images_link3 as $image)                                  
              
                <div class="text-center">
                  <?php
                    echo "<a href=".$image." "."data-lightbox=".'"visit_images_link3"'.">";
                    echo '<img class="" src="'.$image.'" alt="">'."</a>";
                  ?>
                </div>

                <?php $i++; ?>              

              @endforeach
              
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit4Photos">
            <h2 class="text-center"><strong>Visit Photos</strong></h2>
            <h4 class="text-center">Visit 4 </h4>
             <div id="owl-example" class="owl-carousel">

              <?php $i=0; ?>

              @foreach($entity->visit_images_link4 as $image)                                  
              
                <div class="text-center">
                  <?php
                    echo "<a href=".$image." "."data-lightbox=".'"visit_images_link4"'.">";
                    echo '<img class="" src="'.$image.'" alt="">'."</a>";
                  ?>
                </div>

                <?php $i++; ?>              

              @endforeach
              
            </div>
          </div>
        </div>
      </div>
      <!-- /tile body -->
    </section>
  </div>
</div>
<!-- /row -->
@stop
@section('custom_script')
<script type="text/javascript">
//owl carousel
$(".owl-carousel").owlCarousel({
singleItem: true,
autoPlay: true,
navigation: true,
slideSpeed: 400,
paginationSpeed: 500,
navigationText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>']
});
</script>
@stop