@extends('layouts.default.master')
@section('content')

<!-- content main container -->
<div class="main">
  <!-- row -->
  <div class="row">
    <!-- col 12 -->
    <div class="col-md-12">
      <!-- tile -->
      <section class="tile color blue" >
        <!-- tile header -->
        <div class="tile-header transparent">
          @if(Auth::user()->campaign == 'vim' || Auth::user()->campaign == 'rin')
            <h1><strong>Uthans & Haats </strong></h1>
          @elseif(Auth::user()->campaign == 'lux')
            <h1><strong>Uthans & Colleges </strong></h1>
          @endif
          {{-- <span class="note">including: <span class="italic">TableTools and ColVis</span></span> --}}
          <div class="controls">
            <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="remove"><i class="fa fa-times"></i></a>
          </div>
        </div>
        <!-- /tile header -->

        <!-- tile body -->
        <div class="tile-body color blue rounded-corners" style="overflow:auto">
          <div class="table-responsive">
            <table  class="table table-datatable table-custom" id="advancedDataTable">
              <thead>
                <tr>
                  <th><span>agency</span></th>
                  <th><span>date</span></th>
                  <th><span>country</span></th>
                  <th><span>division</span></th>
                  <th><span>district</span></th>
                  <th><span>thana</span></th>
                  <th><span>union</span></th>
                  <th><span>village</span></th>
                  @if(Auth::user()->campaign == 'vim' || Auth::user()->campaign == 'rin')
                    <th><span>uthan_owner_name</span></th>
                    <th><span>uthan_owner_mobile</span></th>
                    <th><span>uthan_participant_name</span></th>
                    <th><span>uthan_participant_mobile</span></th>
                    <th><span>shop_owner_name</span></th>
                    <th><span>shop_owner_mobile</span></th>
                    <th><span>haat_participant_name</span></td>
                    <th><span>haat_participant_mobile</span></th> 
                  @elseif(Auth::user()->campaign == 'lux')
                    <th><span>college_name</span></th>
                    <th><span>principal_name</span></th>
                    <th><span>principal_mobile</span></th>
                    <th><span>teacher_name</span></th>
                    <th><span>teacher_mobile</span></th>
                  @endif 
                  <th><span>total_contact</span></th>
                  <th><span>visit_images1</span></th>
                  <th><span>visit_certificate1</span></th>
                  <th><span>visit_date1</span></th>
                  <th><span>visit_images2</span></th>
                  <th><span>visit_certificate2</span></th>
                  <th><span>visit_date2</span></th>
                  <th><span>visit_images3</span></th>
                  <th><span>visit_certificate3</span></th>
                  <th><span>visit_date3</span></th>                            
                  <th><span>visit_images4</span></th>
                  <th><span>visit_certificate4</span></th>
                  <th><span>visit_date4</span></th>
                  <th><span>completion_certificate</span></th>

                  @if(Auth::user()->role == 'uni_management')
                    <th><span>show</span></th>   
                  @else
                    <th><span>edit</span></th>   
                  @endif
                </tr>
                </thead>
                <tbody>
                  <tr>   
                   @if(Auth::user()->campaign == 'vim' || Auth::user()->campaign == 'rin')              
                    <td></td>
                    <td></td>
                    <td></td>

                   @endif
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td> 
                    <td></td> 
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /tile body -->



        </section>
        <!-- /tile -->


      </div>
      <!-- /col 12 -->



    </div>
    <!-- /row -->



  </div>
  <!-- /content container -->
</div>
<!-- Page content end -->

@stop


@section('custom_script')

{{-- datatable scripts --}}

{{HTML::script('assets/js/vendor/datatables/jquery.dataTables.min.js')}}
{{HTML::script('assets/js/vendor/datatables/ColReorderWithResize.js')}}
{{HTML::script('assets/js/vendor/datatables/colvis/dataTables.colVis.min.js')}}
{{HTML::script('assets/js/vendor/datatables/tabletools/ZeroClipboard.js')}}
{{HTML::script('assets/js/vendor/datatables/tabletools/dataTables.tableTools.min.js')}}
{{HTML::script('assets/js/vendor/datatables/dataTables.bootstrap.js')}}

{{-- datatable scripts --}}

@if(Auth::user()->campaign == 'vim' || Auth::user()->campaign == 'rin')
<script>
  $(function(){

  // Add custom class to pagination div
  $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_bootstrap paging_custom';

  $('div.dataTables_filter input').addClass('form-control');
  $('div.dataTables_length select').addClass('form-control');


  // /****************************************************/
  // /**************** ADVANCED DATATABLE ****************/
  // /****************************************************/

  var oTable04 = $('#advancedDataTable').dataTable({
    "sDom":
      "<'row'<'col-md-4'l><'col-md-4 text-center sm-left'T C><'col-md-4'f>r>"+
      "t"+
      "<'row'<'col-md-4 sm-center'i><'col-md-4'><'col-md-4 text-right sm-center'p>>",
     "oLanguage": {
      "sSearch": ""
    },
    "sAjaxSource":'reportdata',
    "bProcessing": true,
    "bServerSide": true,
    "aoColumns": [
    { "mDataProp" : "agency"},
    { "mDataProp" : "date"},
    { "mDataProp" : "country"},
    { "mDataProp" : "division"},
    { "mDataProp" : "district"},
    { "mDataProp" : "thana"},
    { "mDataProp" : "union"},
    { "mDataProp" : "village"},
    { "mDataProp" : "uthan_owner_name"},
    { "mDataProp" : "uthan_owner_mobile"},
    { "mDataProp" : "uthan_participant_name"},
    { "mDataProp" : "uthan_participant_mobile"},
    { "mDataProp" : "shop_owner_name"},
    { "mDataProp" : "shop_owner_mobile"},
    { "mDataProp" : "haat_participant_name"},
    { "mDataProp" : "haat_participant_mobile"},
    { "mDataProp" : "total_contact"},
    { "mDataProp" : "visit_images1"},
    { "mDataProp" : "visit_certificate1"},
    { "mDataProp" : "visit_date1"},
    { "mDataProp" : "visit_images2"},
    { "mDataProp" : "visit_certificate2"},
    { "mDataProp" : "visit_date2"},
    { "mDataProp" : "visit_images3"},
    { "mDataProp" : "visit_certificate3"},
    { "mDataProp" : "visit_date3"},
    { "mDataProp" : "visit_images4"},
    { "mDataProp" : "visit_certificate4"},
    { "mDataProp" : "visit_date4"},
    { "mDataProp" : "certificate"},
    { "mDataProp" : <?php
  if(Auth::user()->role == 'uni_management')
    echo '"show"';
  else
    echo '"edit"';
?> }
    ],
    "oTableTools": {
      "sSwfPath": "assets/js/vendor/datatables/tabletools/swf/copy_csv_xls_pdf.swf",
      "aButtons": [
        // "copy",
        // "print",
        {
          "sExtends":    "collection",
          "sButtonText": 'Save <span class="caret" />',
          // "aButtons":    [ "csv", "xls", "pdf" ]
          "aButtons":    [ "csv" ]
        }
      ]
    },
    "fnInitComplete": function(oSettings, json) { 
      $('.dataTables_filter input').attr("placeholder", "Search");
    },
    "oColVis": {
      "buttonText": '<i class="fa fa-eye"></i>'
    }
  });

  $('.ColVis_MasterButton').on('click', function(){
    var newtop = $('.ColVis_collection').position().top - 45; 

    $('.ColVis_collection').addClass('dropdown-menu');
    $('.ColVis_collection>li>label').addClass('btn btn-default')     
    $('.ColVis_collection').css('top', newtop + 'px');
  });

  $('.DTTT_button_collection').on('click', function(){
    var newtop = $('.DTTT_dropdown').position().top - 45;   
    $('.DTTT_dropdown').css('top', newtop + 'px');
  });

  //initialize chosen
  $('.dataTables_length select').chosen({disable_search_threshold: 10});

  // Add custom class
  $('div.dataTables_filter input').addClass('form-control');
  $('div.dataTables_length select').addClass('form-control');

  })

</script>
@endif


@if(Auth::user()->campaign == 'lux')

<script>
  $(function(){

  // Add custom class to pagination div
  $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate paging_bootstrap paging_custom';

  $('div.dataTables_filter input').addClass('form-control');
  $('div.dataTables_length select').addClass('form-control');


  // /****************************************************/
  // /**************** ADVANCED DATATABLE ****************/
  // /****************************************************/

  var oTable04 = $('#advancedDataTable').dataTable({
    "sDom":
      "<'row'<'col-md-4'l><'col-md-4 text-center sm-left'T C><'col-md-4'f>r>"+
      "t"+
      "<'row'<'col-md-4 sm-center'i><'col-md-4'><'col-md-4 text-right sm-center'p>>",
     "oLanguage": {
      "sSearch": ""
    },
    "sAjaxSource":'reportdata',
    "bProcessing": true,
    "bServerSide": true,
    "aoColumns": [
    { "mDataProp" : "agency"},
    { "mDataProp" : "date"},
    { "mDataProp" : "country"},
    { "mDataProp" : "division"},
    { "mDataProp" : "district"},
    { "mDataProp" : "thana"},
    { "mDataProp" : "union"},
    { "mDataProp" : "village"},
    { "mDataProp" : "college_name"},
    { "mDataProp" : "principal_name"},
    { "mDataProp" : "principal_mobile"},
    { "mDataProp" : "teacher_name"},
    { "mDataProp" : "teacher_mobile"},    
    { "mDataProp" : "total_contact"},
    { "mDataProp" : "visit_images1"},
    { "mDataProp" : "visit_certificate1"},
    { "mDataProp" : "visit_date1"},
    { "mDataProp" : "visit_images2"},
    { "mDataProp" : "visit_certificate2"},
    { "mDataProp" : "visit_date2"},
    { "mDataProp" : "visit_images3"},
    { "mDataProp" : "visit_certificate3"},
    { "mDataProp" : "visit_date3"},
    { "mDataProp" : "visit_images4"},
    { "mDataProp" : "visit_certificate4"},
    { "mDataProp" : "visit_date4"},
    { "mDataProp" : "certificate"},
    { "mDataProp" : <?php
  if(Auth::user()->role == 'uni_management')
    echo '"show"';
  else
    echo '"edit"';
?> }
    ],
    "oTableTools": {
      "sSwfPath": "assets/js/vendor/datatables/tabletools/swf/copy_csv_xls_pdf.swf",
      "aButtons": [
        // "copy",
        // "print",
        {
          "sExtends":    "collection",
          "sButtonText": 'Save <span class="caret" />',
          // "aButtons":    [ "csv", "xls", "pdf" ]
          "aButtons":    [ "csv" ]
        }
      ]
    },
    "fnInitComplete": function(oSettings, json) { 
      $('.dataTables_filter input').attr("placeholder", "Search");
    },
    "oColVis": {
      "buttonText": '<i class="fa fa-eye"></i>'
    }
  });

  $('.ColVis_MasterButton').on('click', function(){
    var newtop = $('.ColVis_collection').position().top - 45; 

    $('.ColVis_collection').addClass('dropdown-menu');
    $('.ColVis_collection>li>label').addClass('btn btn-default')     
    $('.ColVis_collection').css('top', newtop + 'px');
  });

  $('.DTTT_button_collection').on('click', function(){
    var newtop = $('.DTTT_dropdown').position().top - 45;   
    $('.DTTT_dropdown').css('top', newtop + 'px');
  });

  //initialize chosen
  $('.dataTables_length select').chosen({disable_search_threshold: 10});

  // Add custom class
  $('div.dataTables_filter input').addClass('form-control');
  $('div.dataTables_length select').addClass('form-control');

  })

</script>

@endif


@stop
