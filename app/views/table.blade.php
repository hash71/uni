@extends('layouts.default.master')
@section('content')



<!-- tile -->
<section class="tile transparent">


<!-- tile header -->
<div class="tile-header transparent">
<h1><strong>Advanced</strong> Datatable </h1>
<span class="note">including: <span class="italic">TableTools and ColVis</span></span>
<div class="controls">
  <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
  <a href="#" class="remove"><i class="fa fa-times"></i></a>
</div>
</div>
<!-- /tile header -->

<!-- tile body -->
<div class="tile-body color blue rounded-corners">

<div class="table-responsive">

<table class="table table-datatable table-custom" id="advancedDataTable">
  <thead>
    <tr>
      <th>agency</th>
      <th>date</th>
      <th>country</th>
      <th>division</th>
      <th>district</th>
      <th>thana</th>
      <th>union</th>
      <th>village</th>
      <th>uthan_owner_name</th>
      <th>uthan_owner_mobile</th>
      <th>uthan_participant_name</th>
      <th>uthan_participant_mobile</th>
      <th>shop_owner_name</th>
      <th>shop_owner_mobile</th>
      <th>haat_participant_name</td> 
      <th>haat_participant_mobile</th>
      <th>college_name</th>
      <th>principal_name</th>
      <th>principal_mobile</th>
      <th>teacher_name</th>
      <th>teacher_mobile</th>
      <th>total_contact</th>
      <th>visit1</th>
      <th>visit2</th>
      <th>visit3</th> 
      <th>visit4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>                            
      <td></td>                            
      <td></td>                            
      <td></td>                            
      <td></td>
    </tr>
  </tbody>
</table>

</div>

</div>
<!-- /tile body -->



</section>
<!-- /tile -->











@stop



@section('custom_script')
  <script type="text/javascript">
     var oTable04 = $('#advancedDataTable').dataTable({
        "sAjaxSource":'reportdata',
        "bProcessing": true,
        "bServerSide": true, 
        "sDom":
          "<'row'<'col-md-4'l><'col-md-4 text-center sm-left'T C><'col-md-4'f>r>"+
          "t"+
          "<'row'<'col-md-4 sm-center'i><'col-md-4'><'col-md-4 text-right sm-center'p>>",
         "oLanguage": {
          "sSearch": ""
        },
        "oTableTools": {
          "sSwfPath": "assets/js/vendor/datatables/tabletools/swf/copy_csv_xls_pdf.swf",
          "aButtons": [
            "copy",
            "print",
            {
              "sExtends":    "collection",
              "sButtonText": 'Save <span class="caret" />',
              "aButtons":    [ "csv", "xls", "pdf" ]
            }
          ]
        },
        "fnInitComplete": function(oSettings, json) { 
          $('.dataTables_filter input').attr("placeholder", "Search");
        },
        "oColVis": {
          "buttonText": '<i class="fa fa-eye"></i>'
        }
      });

      $('.ColVis_MasterButton').on('click', function(){
        var newtop = $('.ColVis_collection').position().top - 45; 

        $('.ColVis_collection').addClass('dropdown-menu');
        $('.ColVis_collection>li>label').addClass('btn btn-default')     
        $('.ColVis_collection').css('top', newtop + 'px');
      });

      $('.DTTT_button_collection').on('click', function(){
        var newtop = $('.DTTT_dropdown').position().top - 45;   
        $('.DTTT_dropdown').css('top', newtop + 'px');
      });

      //initialize chosen
      $('.dataTables_length select').chosen({disable_search_threshold: 10});
      
 
      
  </script>
@stop