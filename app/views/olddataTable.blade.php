@extends('layouts.default.master')
@section('content')

<!-- content main container -->
<div class="main">
  <!-- row -->
  <div class="row">
    <!-- col 12 -->
    <div class="col-md-12">
      <!-- tile -->
      <section class="tile color blue" >
        <!-- tile header -->
        <div class="tile-header transparent">
          <h1><strong>Uthans & Haats </strong></h1>
          {{-- <span class="note">including: <span class="italic">TableTools and ColVis</span></span> --}}
          <div class="controls">
            <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="remove"><i class="fa fa-times"></i></a>
          </div>
        </div>
        <!-- /tile header -->

        <!-- tile body -->
        <div class="tile-body rounded-corners" style="overflow:auto">

          <div class="table-responsive">
            <table class="table table-datatable table-hover table-bordered table-custom" id="advancedDataTable">
              <thead>
                <tr>
                  <th>agency</th>
                  <th>date</th>
                  <th>country</th>
                  <th>division</th>
                  <th>district</th>
                  <th>thana</th>
                  <th>union</th>
                  <th>village</th>
                  <th>uthan_owner_name</th>
                  <th>uthan_owner_mobile</th>
                  <th>uthan_participant_name</th>
                  <th>uthan_participant_mobile</th>
                  <th>shop_owner_name</th>
                  <th>shop_owner_mobile</th>
                  <th>haat_participant_name</td> 
                    <th>haat_participant_mobile</th>                    
                    <th>total_contact</th>
                    <th>visit_images1</th>
                    <th>visit_certificate1</th>
                    <th>visit_date1</th>                              
                    <th>visit_images2</th>
                    <th>visit_certificate2</th>                              
                    <th>visit_date2</th>
                    <th>visit_images3</th>
                    <th>visit_certificate3</th>
                    <th>visit_date3</th>                              
                    <th>visit_images4</th>
                    <th>visit_certificate4</th>
                    <th>visit_date4</th>    
                    <th>completion_certificate</th>   
                    <th>edit</th>   
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td>                            
                    <td></td> 
                    <td></td> 
                  </tr>
                </tbody>
              </table>
            </div>

          </div>
          <!-- /tile body -->



        </section>
        <!-- /tile -->


      </div>
      <!-- /col 12 -->



    </div>
    <!-- /row -->



  </div>
  <!-- /content container -->
</div>
<!-- Page content end -->

@stop


@section('custom_script')


{{HTML::script('assets/js/vendor/datatables/jquery.dataTables.min.js')}}
{{HTML::script('assets/js/vendor/datatables/ColReorderWithResize.js')}}

{{HTML::script('assets/js/vendor/datatables/colvis/dataTables.colVis.min.js')}}

{{HTML::script('assets/js/vendor/datatables/tabletools/ZeroClipboard.js')}}
{{HTML::script('assets/js/vendor/datatables/tabletools/dataTables.tableTools.min.js')}}
{{HTML::script('assets/js/vendor/datatables/dataTables.bootstrap.js')}}




{{HTML::style('assets/js/vendor/datatables/css/dataTables.bootstrap.css')}}
{{HTML::style('assets/js/vendor/datatables/css/ColVis.css')}}
{{HTML::style('assets/js/vendor/datatables/css/TableTools.css')}}

{{HTML::script('assets/js/vendor/datatables/jquery.dataTables.min.js')}}

{{HTML::script('assets/js/vendor/datatables/ColReorderWithResize.js')}}
{{HTML::script('assets/js/vendor/datatables/colvis/dataTables.colVis.min.js')}}
{{HTML::script('assets/js/vendor/datatables/tabletools/ZeroClipboard.js')}}
{{HTML::script('assets/js/vendor/datatables/tabletools/dataTables.tableTools.min.js')}}
{{HTML::script('assets/js/vendor/datatables/dataTables.bootstrap.js')}}





<script type="text/javascript">
  /****************************************************/
      /**************** ADVANCED DATATABLE ****************/
      /****************************************************/

      var oTable04 = $('#advancedDataTable').dataTable({
        "bAutoWidth" :false,
        "sAjaxSource":'reportdata',
        "bProcessing": true,
        "bServerSide": true,
        "sDom":
        "<'row'<'col-md-4'l><'col-md-4 text-center sm-left'T C><'col-md-4'f>r>"+
        "t"+
        "<'row'<'col-md-4 sm-center'i><'col-md-4'><'col-md-4 text-right sm-center'p>>",
        "oLanguage": {
          "sSearch": ""
        },
        "aoColumns": [
        { "mDataProp" : "agency"},
        { "mDataProp" : "date"},
        { "mDataProp" : "country"},
        { "mDataProp" : "division"},
        { "mDataProp" : "district"},
        { "mDataProp" : "thana"},
        { "mDataProp" : "union"},
        { "mDataProp" : "village"},
        { "mDataProp" : "uthan_owner_name"},
        { "mDataProp" : "uthan_owner_mobile"},
        { "mDataProp" : "uthan_participant_name"},
        { "mDataProp" : "uthan_participant_mobile"},
        { "mDataProp" : "shop_owner_name"},
        { "mDataProp" : "shop_owner_mobile"},
        { "mDataProp" : "haat_participant_name"},
        { "mDataProp" : "haat_participant_mobile"},
        { "mDataProp" : "total_contact"},
        { "mDataProp" : "visit_images1"},
        { "mDataProp" : "visit_certificate1"},
        { "mDataProp" : "visit_date1"},
        { "mDataProp" : "visit_images2"},
        { "mDataProp" : "visit_certificate2"},
        { "mDataProp" : "visit_date2"},
        { "mDataProp" : "visit_images3"},
        { "mDataProp" : "visit_certificate3"},
        { "mDataProp" : "visit_date3"},
        { "mDataProp" : "visit_images4"},
        { "mDataProp" : "visit_certificate4"},
        { "mDataProp" : "visit_date4"},
        { "mDataProp" : "certificate"},
        { "mDataProp" : "edit"}
        ],    
        "columnDefs": [
         { "class" : "col1", 
         "targets": 1 }
        ],
        "oTableTools": {
          "sSwfPath": "assets/js/vendor/datatables/tabletools/swf/copy_csv_xls_pdf.swf",
          "aButtons": [
          "copy",
          "print",
          {
            "sExtends":    "collection",
            "sButtonText": 'Save <span class="caret" />',
            "aButtons":    [ "csv", "xls", "pdf" ]
          }
          ]
        },

        "fnInitComplete": function(oSettings, json) { 
          $('.dataTables_filter input').attr("placeholder", "Search");
        },
        "oColVis": {
          "buttonText": '<i class="fa fa-eye"></i>'
        }
      });

      $('.ColVis_MasterButton').on('click', function(){
        var newtop = $('.ColVis_collection').position().top - 45; 

        $('.ColVis_collection').addClass('dropdown-menu');
        $('.ColVis_collection>li>label').addClass('btn btn-success')     
        $('.ColVis_collection').css('top', newtop + 'px');
      });

      $('.DTTT_button_collection').on('click', function(){
        var newtop = $('.DTTT_dropdown').position().top - 45;   
        $('.DTTT_dropdown').css('top', newtop + 'px');
      });

      //initialize chosen
      $('.dataTables_length select').chosen({disable_search_threshold: 10});
      
    
</script>



@stop
