@extends('layouts.default.master')
@section('content')
<!-- content main container -->
<div class="main">




  <!-- row -->
  <div class="row">

    <!-- col 12 -->
    <div class="col-md-12">



      <!-- tile -->
      <section class="tile transparent">



        <!-- tile header -->
        <div class="tile-header transparent">
          <h1><strong>Campaigns</strong></h1>
          <div class="controls">
            <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
            <a href="#" class="remove"><i class="fa fa-times"></i></a>
          </div>
        </div>
        <!-- /tile header -->

        <!-- tile body -->
        <div class="tile-body color transparent-black rounded-corners">

          <!-- cards -->
          <div class="row cards">

            <div class="card-container col-lg-3 col-sm-6 col-sm-12">
              <div class="card card-primary hover">
                <div class="front">
                  <div class="media">
                    <div class="media-body">
                      <div class="itemLogo">
                        <a href=""> 
                          <img class="img-responsive" src="assets/images/product-logo/vim.png" alt=""></a>
                        </div>
                      </div>
                    </div> 
                  </div>
                  <div class="back back-primary back-height">
                  @if(Auth::user()->campaign == 'vim')
                    <a href="{{URL::to('dataTable')}}">
                  @else
                    <a href="#">
                  @endif
                      <i class="fa fa-tag fa-4x"></i>
                      <h1 style="color:#717171 !important">Vim Rural Activity</h1>
                    </a>
                  </div>
                </div>
              </div>

              <div class="card-container col-lg-3 col-sm-6 col-sm-12">
                <div class="card card-red hover">
                  <div class="front">
                    <div class="media">
                      <div class="media-body">
                        <div class="itemLogo">
                          <a href="">
                            <img class="img-responsive" src="assets/images/product-logo/rin.png" alt=""></a>
                          </div>
                        </div>
                      </div> 
                    </div>
                    <div class="back back-height">
                      @if(Auth::user()->campaign == 'rin')
                        <a href="{{URL::to('dataTable')}}">
                      @else
                        <a href="#">
                      @endif
                        <i class="fa fa-tag fa-4x"></i>
                        <h1 style="color:#717171 !important">Rin Rural Activity</h1>
                      </a>
                    </div>
                  </div>
                </div>

                <div class="card-container col-lg-3 col-sm-6 col-sm-12">
                  <div class="card card-lightPink hover">
                    <div class="front">
                      <div class="media">
                        <div class="media-body">
                          <div class="itemLogo">
                            <a href="">
                              <img class="img-responsive" src="assets/images/product-logo/lux.png" alt=""></a>
                            </div>
                          </div>
                        </div> 
                      </div>
                      <div class="back back-lightPink back-height">
                        @if(Auth::user()->campaign == 'lux')
                          <a href="{{URL::to('dataTable')}}">
                        @else
                          <a href="#">
                        @endif
                          <i class="fa fa-tag fa-4x"></i>
                          <h1 style="color:#717171 !important">LUX Rural Activity</h1>
                        </a>
                      </div>
                    </div>
                  </div>

                  <div class="card-container col-lg-3 col-sm-6 col-sm-12">
                    <div class="card card-white hover">
                      <div class="front">
                        <div class="media">
                          <div class="media-body">
                            <div class="itemLogo">
                              <a href="">
                                <img class="img-responsive" src="assets/images/product-logo/dove.png" alt=""></a>
                              </div>
                            </div>
                          </div> 
                        </div>
                        <div class="back back-orange back-height">
                          <a href="#">
                            <i class="fa fa-tag fa-4x"></i>
                            <h2 style="color:#717171 !important">D2D Face Wash Activity</h2>
                          </a>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- /cards -->

                </div>
                <!-- /tile body -->

                


              </section>
              <!-- /tile -->




            </div>
            <!-- /col 12 -->



          </div>
          <!-- /row -->


</div>
<!-- /content container -->

@stop

@section('custom_script')
    <script>
    $(function(){

      // Initialize card flip
      $('.card.hover').hover(function(){
        $(this).addClass('flip');
      },function(){
        $(this).removeClass('flip');
      });

      // //show tooltips
      // $('#topTooltip, #rightTooltip, #bottomTooltip, #leftTooltip').tooltip();

      // //jGrowl notifications
      // $("#defaultGrowl").click(function() {
      //   $.jGrowl("Hello world!");
      // });

      // $("#stickyGrowl").click(function() {
      //   $.jGrowl("Stick this!", { sticky: true });
      // });

      // $("#headerGrowl").click(function() {
      //   $.jGrowl("A message with a header", { header: 'Important' });
      // });

      // $("#longerGrowl").click(function() {
      //   $.jGrowl("A message that will live a little longer.", { life: 10000 });
      // });

      // $("#specialGrowl").click(function() {
      //   $.jGrowl("A message with a beforeClose callback and a different opening animation.", {
      //     beforeClose: function(e,m) {
      //       alert('About to close this notification!');
      //     },
      //     animateOpen: {
      //       height: 'show'
      //     }
      //   });
      // });

      // // Initialize tabDrop
      // $('.tabdrop').tabdrop({text: '<i class="fa fa-th-list"></i>'});

      // //initialize typeahead
      // $('#typeahead').typeahead({
      //   name: 'States',
      //   local: ["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]
      // });

      // //initialize datepicker
      // $('#datepicker').datetimepicker({
      //   icons: {
      //     time: "fa fa-clock-o",
      //     date: "fa fa-calendar",
      //     up: "fa fa-arrow-up",
      //     down: "fa fa-arrow-down"
      //   }
      // });

      // $("#datepicker").on("dp.show",function (e) {
      //   var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;      
      //   $('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
      // });

      // //initialize chosen
      // $(".chosen-select").chosen({disable_search_threshold: 10});

      // //initialize range slider
      // $('#rangeSlider').noUiSlider({
      //   range: [10,40],
      //   start: [20,30],
      //   connect: true
      // });

      // //initialize slider
      // $('#slider').noUiSlider({
      //   range: [0,100],
      //   start: [20],
      //   handles: 1
      // });

      // //initialize color picker sliders
      // $('.slider').noUiSlider({
      //    range: [0,255]
      //   ,start: 127
      //   ,handles: 1
      //   ,connect: "lower"
      //   ,orientation: "vertical"
      //   ,serialization: {
      //     resolution: 1
      //   }
      //   ,slide: function(){

      //     var color = 'rgb(' + $("#red").val()
      //        + ',' + $("#green").val()
      //        + ',' + $("#blue").val()
      //        + ')';

      //     $(".result").css({
      //        background: color
      //       ,color: color
      //     });
      //   }
      // });

      // //set width for label on Inline Select
      // var setLabelWidth = function() {
      //   var parentWidth = $('.inlineSelect.inline').width();
      //   var childrenLength = $('.inlineSelect.inline li').length;

      //   $('.inlineSelect.inline li label, .inlineSelect.inline li.title').css('width', ((parentWidth / childrenLength)) + 'px');
      // }

      // setLabelWidth();

      // $(window).resize(function() {
      //   setLabelWidth();
      // });

      // //accordion class active toggling
      // $('#accordion .panel-heading .panel-title a').click(function() {

      //   var $previous = $( '#accordion .panel.active' );

      //   $previous.removeClass('active');
      //   $(this).parent().parent().parent().stop().addClass('active');

      //   if($(this).parent().parent().parent().hasClass('active')) {
      //     $previous.removeClass('active');
      //   }
      // });

      // //multi-accordion class active toggling
      // $('#multi-accordion .panel-heading .panel-title a').click(function() {
      //   $(this).parent().parent().parent().stop().toggleClass('active');
      // });
      
    })
      
    </script>
@stop




