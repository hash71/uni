<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

{{HTML::script('assets/js/vendor/jquery/jquery.min.js')}}

<!-- Include all compiled plugins (below), or include individual files as needed -->

{{HTML::script('assets/js/vendor/bootstrap/bootstrap.min.js')}}


{{HTML::script('assets/js/vendor/mmenu/js/jquery.mmenu.min.js')}}
{{HTML::script('assets/js/vendor/sparkline/jquery.sparkline.min.js')}}
{{HTML::script('assets/js/vendor/nicescroll/jquery.nicescroll.min.js')}}
{{HTML::script('assets/js/vendor/animate-numbers/jquery.animateNumbers.js')}}
{{HTML::script('assets/js/vendor/videobackground/jquery.videobackground.js')}}
{{HTML::script('assets/js/vendor/blockui/jquery.blockUI.js')}}
{{HTML::script('assets/js/vendor/momentjs/moment-with-langs.min.js')}}
{{HTML::script('assets/js/vendor/datepicker/bootstrap-datetimepicker.min.js')}}

{{HTML::script('assets/js/vendor/chosen/chosen.jquery.min.js')}}

{{HTML::script('assets/js/vendor/parsley/parsley.min.js')}}

{{HTML::script('assets/js/vendor/validate/validate.min.js')}}
{{HTML::script('assets/js/vendor/validate/validate.additional.min.js')}}

{{HTML::script('assets/js/minimal.min.js')}}

{{HTML::script('assets/js/lightbox.min.js')}}



{{HTML::script('assets/js/vendor/owl-carousel/owl.carousel.min.js')}}






<script>

	$(function(){
		$(".chosen-select").chosen({disable_search_threshold: 10});

		$('.datepickermulti').datetimepicker({
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			},
			format: 'YYYY-MM-DD'
		});

		$(".datepicker").on("dp.show",function (e) {
			var newtop = $('.bootstrap-datetimepicker-widget').position().top - 45;      
			$('.bootstrap-datetimepicker-widget').css('top', newtop + 'px');
		});

		$(document).on('change', '.btn-file :file', function() {
			var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, 'http://tattek.com/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		});

	    //initialize file upload button
	    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

	    	var input = $(this).parents('.input-group').find(':text'),
	    	log = numFiles > 1 ? numFiles + ' files selected' : label;


	    	console.log(log);
	    	// alert(this.files[0].size);	
	    	if( input.length ) {
	    		input.val(log);
	    	} else {
	    		if( log ) alert(log);
	    	}
	    	if(this.files[0].size > 2097152){
	    		alert('file size exceeded');
	    		$(this).val('');

	    		$(this).closest('.input-group').find(':text').val('');
	    	}
	    	

	    });
	});

    </script>