        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top navbar-amethyst mm-fixed-top" role="navigation" id="navbar">
          <!-- Branding -->
          <div class="navbar-header col-md-2">
            <a class="navbar-brand" href="{{URL::to('dashboard')}}">
             <!--  <img class="img-responsive" src="assets/images/logo.png" alt=""> -->
            </a>
            <div class="sidebar-collapse">
              <a href="#">
                <i class="fa fa-bars"></i>
              </a>
            </div>
          </div>
          <!-- Branding end -->


          <!-- .nav-collapse -->
          <div class="navbar-collapse">
                        
            <!-- Page refresh -->
           {{--  <ul class="nav navbar-nav refresh">
              <li class="divided">
                <a href="#" class="page-refresh"><i class="fa fa-refresh"></i></a>
              </li>
            </ul> --}}
            <!-- /Page refresh -->

            <!-- Quick Actions -->
            <ul class="nav navbar-nav quick-actions">


              <li class="dropdown divided user" id="current-user">
                <div class="profile-photo">
                  {{HTML::image('assets/images/profile-photo.jpg')}}                  
                </div>
                <a class="dropdown-toggle options" data-toggle="dropdown" href="#">
                  {{Auth::user()->username}} <i class="fa fa-caret-down"></i>
                </a>
                
                <ul class="dropdown-menu arrow settings">



                  {{-- <li class="divider"></li>

                  <li>
                    <a href="#"><i class="fa fa-user"></i> Profile</a>
                  </li>

                  <li>
                    <a href="#"><i class="fa fa-calendar"></i> Calendar</a>
                  </li>

                  <li>
                    <a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge badge-red" id="user-inbox">3</span></a>
                  </li>

                  <li class="divider"></li> --}}

                  <li>    
                    <a href="{{URL::to('logout')}}"><i class="fa fa-power-off"></i> Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
            <!-- /Quick Actions -->

            <!-- Sidebar -->
            <ul class="nav navbar-nav side-nav" id="sidebar">
              
              <li class="collapsed-content"> 
                <ul>
                  <li class="search"><!-- Collapsed search pasting here at 768px --></li>
                </ul>
              </li>
              
              @if(!isset($page))                
              <li class="navigation" id="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="#navigation">Navigation <i class="fa fa-angle-up"></i></a>
                
                <ul class="menu">  
                
                  <li>
                    <a class="bg-red" href="{{URL::to('dashboard')}}">
                      <i class="fa fa-laptop"></i> Dashboard
                    </a>
                  </li>
                  @if(Auth::user()->role == 'operator')
                    <li>
                      <a class="bg-drank" href="{{URL::to('form/uthan')}}">
                        <i class="fa fa-pencil-square-o"></i> Create Uthan
                      </a>
                    </li>
                    @if(Auth::user()->campaign != 'lux')
                    <li>
                      <a class="bg-blue" href="{{URL::to('form/haat')}}">
                        <i class="fa fa-pencil-square-o"></i> Create Haat
                      </a>
                    </li>
                    @endif

                    @if(Auth::user()->campaign == 'lux')
                      <li>
                        <a class="bg-blue" href="{{URL::to('form/college')}}">
                          <i class="fa fa-laptop"></i> Create College
                        </a>
                      </li>
                    @endif

                  @endif
                  <li>
                      <a class="bg-green" href="{{URL::to('dataTable')}}">
                        <i class="fa fa-file-text"></i> Full Report
                      </a>
                    </li>
                  
                </ul>

              </li>
              @endif
              
            </ul>
            <!-- Sidebar end -->
          </div>
          <!--/.nav-collapse -->
        </div>
        <!-- Fixed navbar end -->