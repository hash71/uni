@extends('layouts.default.master')
@section('content')
<!-- row -->
<div class="row">
  <!-- col 12 -->
  <div class="col-md-8">
    <!-- tile -->
    <section class="tile color blue" >
      <!-- tile header -->
      <div class="tile-header color rounded-top-corners">
        <h1><strong>{{ucfirst($entity->type)}} Details</strong></h1>
        <div class="controls">
          <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
          <a href="#" class="remove"><i class="fa fa-times"></i></a>
        </div>
      </div>
      <!-- /tile header -->
      <!-- tile body -->
      <div class="tile-body">
        <form class="form-horizontal editForm" role="form" id="basicvalidations" action="{{URL::to('editForm',$entity->_id)}}" method="post">
          <div class="form-group odd">
            <label for="agency" class="col-sm-4 control-label">Agency *</label>
            <div class="col-sm-8" id="selectbox">
              <select class="chosen-select chosen-transparent form-control" id="" required="true" name="agency">
                @foreach(Agency::lists('name') as $agency)
                <option <?php if($agency == $entity->agency) echo "selected"; ?> value="{{$agency}}">{{$agency}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group even">
            <label for="datepicker" class="col-sm-4 control-label">Date *</label>
            <div class="col-sm-8" >
              <input type="text" class="form-control datepickermulti" id="" name="date" required="true" value="{{$entity->date}}">

            </div>
          </div>
          <div class="form-group odd">
            <label for="country" class="col-sm-4 control-label">Country *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="country" readonly value="Bangladesh" name="country">
            </div>
          </div>
          <div class="form-group even">
            <label for="division" class="col-sm-4 control-label">Division *</label>
            <div class="col-sm-8" id="selectbox">
              <select class="chosen-select chosen-transparent form-control" id="division" required="true" name="division">
                <option value="">Please choose</option>
                @foreach(Area::distinct('division')->get() as $division)
                <option value="{{$division[0]}}" <?php if($division[0]==$entity->division) echo "selected";?>>{{$division[0]}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group odd">
            <label for="district" class="col-sm-4 control-label">District *</label>
            <div class="col-sm-8" id="selectbox">
              <select class="chosen-select chosen-transparent form-control" id="district" required="true" name="district">
                <option value="">Please choose</option>
                <option value="{{$entity->district}}" selected>{{$entity->district}}</option>
              </select>
            </div>
          </div>
          <div class="form-group even">
            <label for="thana" class="col-sm-4 control-label">Thana *</label>
            <div class="col-sm-8" id="selectbox">
              <select class="chosen-select chosen-transparent form-control" id="thana" required="true" name="thana">
                <option value="">Please choose</option>
                <option value="{{$entity->thana}}" selected>{{$entity->thana}}</option>
              </select>
            </div>
          </div>
          <div class="form-group odd">
            <label for="ownerName" class="col-sm-4 control-label">Union *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->union}}" required="true" name="union">
            </div>
          </div>
          <div class="form-group even">
            <label for="ownerName" class="col-sm-4 control-label">Village *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->union}}" required="true" name="village">
            </div>
          </div>
          @if($entity->type == 'uthan')
          <div class="form-group odd">
            <label for="" class="col-sm-4 control-label">Uthan Owner's Name</label>
            <div class="col-sm-8" id="selectbox">
              <input type="text" class="form-control" id="" value="{{$entity->uthan_owner_name}}" required="true" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" name="uthan_owner_name">
            </div>
          </div>
          <div class="form-group even">
            <label for="ownerName" class="col-sm-4 control-label">Uthan Owner's Cell *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->uthan_owner_mobile}}" required="true" pattern="^01[5-9]\d{8}$" name="uthan_owner_mobile">
            </div>
          </div>
          <div class="form-group odd">
            <label for="phonenum" class="col-sm-4 control-label">Uthan Participant's Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->uthan_participant_name}}" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" required="true" name="uthan_participant_name">
            </div>
          </div>
          <div class="form-group even">
            <label for="phonenum" class="col-sm-4 control-label">Uthan Participant's Cell *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="phonenum" value="{{$entity->uthan_participant_mobile}}" pattern="^01[5-9]\d{8}$" required="true" name="uthan_participant_mobile">
            </div>
          </div>
          @endif
          @if($entity->type == 'haat')
          <div class="form-group odd">
            <label for="ownerName" class="col-sm-4 control-label">Shop Owner's Name *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->shop_owner_name}}" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" required="true" name="shop_owner_name">
            </div>
          </div>
          <div class="form-group even">
            <label for="ownerName" class="col-sm-4 control-label">Shop Owner's Cell *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->shop_owner_mobile}}" pattern="^01[5-9]\d{8}$" required="true" name="shop_owner_mobile">
            </div>
          </div>
          <div class="form-group odd">
            <label for="phonenum" class="col-sm-4 control-label">Haat Participant's Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->haat_participant_name}}" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" required="true" name="haat_participant_name">
            </div>
          </div>
          <div class="form-group even">
            <label for="phonenum" class="col-sm-4 control-label">Haat Participant's Cell *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="phonenum" value="{{$entity->haat_participant_mobile}}" pattern="^01[5-9]\d{8}$" required="true" name="haat_participant_mobile">
            </div>
          </div>
          @endif
          @if($entity->type == 'college')
          <div class="form-group odd">
            <label for="datepicker" class="col-sm-4 control-label">College Name</label>
            <div class="col-sm-8" id="selectbox">
              <input type="text" class="form-control" id="" value="{{$entity->college_name}}" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" required="true" name="college_name">
            </div>
          </div>
          <div class="form-group even">
            <label for="ownerName" class="col-sm-4 control-label">Principal Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->principal_name}}" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" required="true" name="principal_name">
            </div>
          </div>
          <div class="form-group odd">
            <label for="phonenum" class="col-sm-4 control-label">Principal's Cell *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->principal_mobile}}" pattern="^01[5-9]\d{8}$" required="true" name="principal_mobile">
            </div>
          </div>
          <div class="form-group even">
            <label for="phonenum" class="col-sm-4 control-label">Teacher Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="" value="{{$entity->teacher_name}}" required="true" name="teacher_name">
            </div>
          </div>
          <div class="form-group odd">
            <label for="phonenum" class="col-sm-4 control-label">Teacher's Cell *</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id=""  value="{{$entity->teacher_mobile}}" pattern="^01[5-9]\d{8}$" required="true" name="teacher_mobile">
            </div>
          </div>
          @endif
          <div class="form-group even">
            <label for="numval" class="col-sm-4 control-label">Total Contact</label>
            <div class="col-sm-8">
              <input type="number" required class="form-control" id="numval" placeholder="must be a number..." pattern="\d*" name="total_contact" value="{{$entity->total_contact}}" min="1" >
            </div>
          </div>
          <div class="form-group form-footer bg-white">
            <div class="col-sm-4">
              <button type="submit" class="btn btn-lg btn-blue ">Update</button>              
            </div>
          </div>
        </form>
        
      </div>
      <!-- /tile body -->
    </section>
    <!-- /tile -->
    <div class="bara text-center">
    <?php
        $role = Auth::user()->role;
    ?>
    @if(($role == 'agency_admin' && $entity->uni_admin!=1) ||($role == 'uni_admin' && $entity->uni_management!=1))
    <form action="{{URL::to('approval')}}" method="post">
      <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
      <button type="submit" class="btn btn-lg btn-success text-center">Approve</button>
    </form>
    @endif
      
    </div>
    
  </div>
  <!-- /col 9 -->
  <div class="col-md-4">
    <section class="tile color" style="background-color:#5D6967">
      <!-- tile header -->
      <div class="tile-header text-center">
        <h1><strong>Completion Certificate</strong></h1>
        <hr>
      </div>
      <!-- /tile header -->
      <!-- tile body -->
      <form id="certificate_link" action="" enctype="multipart/form-data" method="post">
        <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
        <div class="tile-body img-holder">
          @if($entity->certificate_link)
          <a class="img-delete btn btn-red btn-sm color-black pull-right" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
            <i class="fa fa-trash"></i>
            
            Delete
            
          </a>
          @endif
          <div class="clearfix"></div>
          <input class="certificate_delete" type="hidden" value="certificate_link+certificate">
          <div class="certImg">
            <?php
            echo "<a href=".$entity->certificate_link." "."data-lightbox=".'"certificate_link"'.">";
            echo '<img class="img-responsive" src="'.$entity->certificate_link.'" alt="">'."</a>";
            ?>
          </div>
        </div>
        <!-- /tile body -->
        <div class="tile-footer">
          <a class="btn bg-white btn-md color-black btn-file">
            <i class="fa fa-upload"></i><input type="file" name="image_file" accept="image/png,image/jpg,image/jpeg">
            Upload
          </a>
          <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>
        </div>
      </form>
    </section>
    <section class="tile color" style="background-color:#A63F82">
      <!-- tile widget -->
      <div class="tile-widget nopadding color transparent-black rounded-top-corners">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#visit1" aria-controls="visit1" role="tab" data-toggle="tab">Visit  1</a></li>
          <li role="presentation"><a href="#visit2" aria-controls="visit2" role="tab" data-toggle="tab">Visit  2</a></li>
          <li role="presentation"><a href="#visit3" aria-controls="visit3" role="tab" data-toggle="tab">Visit  3</a></li>
          <li role="presentation"><a href="#visit4" aria-controls="visit4" role="tab" data-toggle="tab">Visit  4</a></li>
        </ul>
        <!-- / Nav tabs -->
      </div>
      <!-- /tile widget -->
      <!-- tile body -->
      <div class="tile-body rounded-bottom-corners">
        <div class="form-horizontal">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="visit1">
              <form id="visit_id_1" action="" enctype="multipart/form-data" method="post">
                <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
                <h2 class="text-center" style="margin-bottom:15px;"><strong>Visit Information</strong></h2>
                <div class="form-group ">
                  <label for="datepicker" class="col-sm-4 control-label">Date</label>
                  <div class="col-sm-8">
                    <input name="date" type="text" class="form-control datepickermulti" id="datepicker" value="{{$entity->visit_date1}}">
                  </div>
                </div>
                <p class="text-center" style="margin-top:15px;">ACKNOWLEDGEMENT CERTIFICATES</p>
                <div class="img-holder">
                  @if($entity->visit_certificate_link1)
                  <a class="img-delete btn btn-danger btn-sm color-black pull-right" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
                    <i class="fa fa-trash"></i>
                    Delete
                  </a>
                  @endif
                  <input class="certificate_delete" type="hidden" value="visit_certificate_link1+visit_certificate1">
                  <div class="clearfix"></div>
                  <div class="clearfix"></div>
                  <div class="certImg">
                    <?php
                    echo "<a href=".$entity->visit_certificate_link1." "."data-lightbox=".'"visit_certificate_link1"'.">";
                    echo '<img class="img-responsive" src="'.$entity->visit_certificate_link1.'" alt="">'."</a>";
                    ?>
                  </div>
                </div>
                <div class="margin-top-20" >
                  <a class="btn bg-white color-black btn-file">
                    <i class="fa fa-upload"></i><input type="file" name="image_file" accept="image/png,image/jpg,image/jpeg">
                    Upload
                  </a>
                  <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>
                </div>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="visit2">
              <form id="visit_id_2" action="" enctype="multipart/form-data" method="post">
                <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
                <h2 class="text-center" style="margin-bottom:15px;"><strong>Visit Information</strong></h2>
                <div class="form-group ">
                  <label for="datepicker" class="col-sm-4 control-label">Date</label>
                  <div class="col-sm-8">
                    <input name="date" type="text" class="form-control datepickermulti" id="datepicker" value="{{$entity->visit_date2}}">
                  </div>
                </div>
                <p class="text-center" style="margin-top:15px;">ACKNOWLEDGEMENT CERTIFICATES</p>
                <div class="img-holder">
                  @if($entity->visit_certificate_link2)
                  <a class="img-delete btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
                    <i class="fa fa-trash"></i>
                    Delete
                    <input class="certificate_delete" type="hidden" value="visit_certificate_link2+visit_certificate2">
                  </a>
                  @endif
                  <div class="clearfix"></div>
                  <div class="certImg">
                    <?php
                    echo "<a href=".$entity->visit_certificate_link2." "."data-lightbox=".'"visit_certificate_link2"'.">";
                    echo '<img class="img-responsive" src="'.$entity->visit_certificate_link2.'" alt="">'."</a>";
                    ?>
                  </div>
                </div>
                <div class="margin-top-20" >
                  <a class="btn bg-white color-black btn-file">
                    <i class="fa fa-upload"></i><input type="file" name="image_file" accept="image/png,image/jpg,image/jpeg">
                    Upload
                  </a>
                  <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>

                </div>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="visit3">
              <form id="visit_id_3" action="" enctype="multipart/form-data" method="post">
                <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
                <h2 class="text-center" style="margin-bottom:15px;"><strong>Visit Information</strong></h2>
                <div class="form-group ">
                  <label for="datepicker" class="col-sm-4 control-label">Date</label>
                  <div class="col-sm-8">
                    <input name="date" type="text" class="form-control datepickermulti" id="datepicker" value="{{$entity->visit_date3}}">
                  </div>
                </div>
                <p class="text-center" style="margin-top:15px;">ACKNOWLEDGEMENT CERTIFICATES</p>
                <div class="img-holder">
                @if($entity->visit_certificate_link3)
                  <a class="img-delete btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">                  
                    <i class="fa fa-trash"></i>
                    Delete
                    <input class="certificate_delete" type="hidden" value="visit_certificate_link3+visit_certificate3">
                  </a>
                  @endif
                  <div class="clearfix"></div>
                  <div class="certImg">
                    <?php
                    echo "<a href=".$entity->visit_certificate_link3." "."data-lightbox=".'"visit_certificate_link3"'.">";
                    echo '<img class="img-responsive" src="'.$entity->visit_certificate_link3.'" alt="">'."</a>";
                    ?>
                  </div>
                </div>
                <div class="margin-top-20" >
                  <a class="btn bg-white color-black btn-file">
                    <i class="fa fa-upload"></i><input type="file" name="image_file" accept="image/png,image/jpg,image/jpeg">
                    Upload
                  </a>
                  <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>

                </div>
              </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="visit4">
              <form id="visit_id_4" action="" enctype="multipart/form-data" method="post">
                <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
                <h2 class="text-center" style="margin-bottom:15px;"><strong>Visit Information</strong></h2>
                <div class="form-group ">
                  <label for="datepicker" class="col-sm-4 control-label">Date</label>
                  <div class="col-sm-8">
                    <input name="date" type="text" class="form-control datepickermulti" id="datepicker" value="{{$entity->visit_date4}}">
                  </div>
                </div>
                <p class="text-center" style="margin-top:15px;">ACKNOWLEDGEMENT CERTIFICATES</p>
                <div class="img-holder">
                @if($entity->visit_certificate_link4)
                  <a class="img-delete btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
                    
                    <i class="fa fa-trash"></i>
                    Delete
                    <input class="certificate_delete" type="hidden" value="visit_certificate_link4+visit_certificate4">
                  </a>
                  @endif
                  <div class="clearfix"></div>
                  <div class="certImg">
                    <?php
                    echo "<a href=".$entity->visit_certificate_link4." "."data-lightbox=".'"visit_certificate_link4"'.">";
                    echo '<img class="img-responsive" src="'.$entity->visit_certificate_link4.'" alt="">'."</a>";
                    ?>
                  </div>
                </div>
                <div class="margin-top-20" >
                  <a class="btn bg-white color-black btn-file">
                    <i class="fa fa-upload"></i><input type="file" name="image_file" accept="image/png,image/jpg,image/jpeg">
                    Upload
                  </a>
                  <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /tile body -->
    </section>
    <section class="tile color certificate" style="background-color:#237676">
      <!-- tile widget -->
      <div class="tile-widget nopadding color transparent-black rounded-top-corners">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#visit1Photos" aria-controls="visit1Photos" role="tab" data-toggle="tab">Visit 1</a></li>
          <li role="presentation"><a href="#visit2Photos" aria-controls="visit2Photos" role="tab" data-toggle="tab">Visit 2</a></li>
          <li role="presentation"><a href="#visit3Photos" aria-controls="visit3Photos" role="tab" data-toggle="tab">Visit 3</a></li>
          <li role="presentation"><a href="#visit4Photos" aria-controls="visit4Photos" role="tab" data-toggle="tab">Visit 4</a></li>
        </ul>
        <!-- / Nav tabs -->
      </div>
      <!-- /tile widget -->
      <!-- tile body -->
      <div class="tile-body rounded-bottom-corners" style="overflow:hidden">
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="visit1Photos">
            <form id="gallery_link_1" action="{{URL::to('multi_image_upload',1)}}" enctype="multipart/form-data" method="post" >
              <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
              <h2 class="text-center"><strong>Visit Photos</strong></h2>
              <h4 class="text-center">Visit 1 </h4>
              <?php $i=0; ?>
              @foreach($entity->visit_images_link1 as $image)
              <div class="single-slider-img gallery-holder">
                <a class="gallery-delete1 btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
                  <i class="fa fa-trash"></i>
                  Delete
                  <input class="certificate_delete" type="hidden" value="visit_images_link1+visit_images1+{{$i}}">
                </a>
                <div class="clearfix"></div>
                <div class="certImg">
                  <?php
                  echo "<a href=".$image." "."data-lightbox=".'"visit_images_link1"'.">";
                  echo '<img class="img-responsive" src="'.$image.'" alt="">'."</a>";
                  ?>
                </div>
              </div>
              <?php $i++; ?>
              @endforeach
              <div class="clearfix"></div>
              <div class="margin-top-20" >
                <a class="btn bg-white color-black btn-file">
                  <i class="fa fa-upload"></i><input type="file" multiple="" name="upload_files[]" accept="image/png,image/jpg,image/jpeg">
                  Upload
                </a>
                <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>
              </div>
            </form>
          </div>
          <div role="tabpanel" class="tab-pane" id="visit2Photos">
            <form id="gallery_link_2" action="{{URL::to('multi_image_upload',2)}}" enctype="multipart/form-data" method="post" >
            <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
            <h2 class="text-center"><strong>Visit Photos</strong></h2>
            <h4 class="text-center">Visit 2 </h4>
            <?php $i=0; ?>
            @foreach($entity->visit_images_link2 as $image)
            <div class="single-slider-img gallery-holder">
              <a class="gallery-delete2 btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
                <i class="fa fa-trash"></i>
                Delete
                <input class="certificate_delete" type="hidden" value="visit_images_link2+visit_images2+{{$i}}">
              </a>
              <div class="clearfix"></div>
              <div class="certImg">
                <?php
                echo "<a href=".$image." "."data-lightbox=".'"visit_images_link2"'.">";
                echo '<img class="img-responsive" src="'.$image.'" alt="">'."</a>";
                ?>
              </div>
            </div>
            <?php $i++; ?>
            @endforeach
            <div class="clearfix"></div>
            <div class="margin-top-20" >
              <a class="btn bg-white color-black btn-file">
                <i class="fa fa-upload"></i><input type="file" multiple="" name="upload_files[]" accept="image/png,image/jpg,image/jpeg">
                Upload
              </a>
              <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>
            </div>
          </form></div>
          <div role="tabpanel" class="tab-pane" id="visit3Photos">
            <form id="gallery_link_3" action="{{URL::to('multi_image_upload',3)}}" enctype="multipart/form-data" method="post" >
            <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
            <h2 class="text-center"><strong>Visit Photos</strong></h2>
            <h4 class="text-center">Visit 3 </h4>
            <?php $i=0; ?>
            @foreach($entity->visit_images_link3 as $image)
            <div class="single-slider-img gallery-holder">
              <a class="gallery-delete3 btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
                <i class="fa fa-trash"></i>
                Delete
                <input class="certificate_delete" type="hidden" value="visit_images_link3+visit_images3+{{$i}}">
              </a>
              <div class="clearfix"></div>
              <div class="certImg">
                <?php
                echo "<a href=".$image." "."data-lightbox=".'"visit_images_link3"'.">";
                echo '<img class="img-responsive" src="'.$image.'" alt="">'."</a>";
                ?>
              </div>
            </div>
            <?php $i++; ?>
            @endforeach
            <div class="clearfix"></div>
            <div class="margin-top-20" >
              <a class="btn bg-white color-black btn-file">
                <i class="fa fa-upload"></i><input type="file" multiple="" name="upload_files[]" accept="image/png,image/jpg,image/jpeg">
                Upload
              </a>
              <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>
            </div>
          </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit4Photos">
          <form id="gallery_link_4" action="{{URL::to('multi_image_upload',4)}}" enctype="multipart/form-data" method="post" >
          <input type="hidden" value="{{$entity->_id}}" name="__id" class="entityid" >
          <h2 class="text-center"><strong>Visit Photos</strong></h2>
          <h4 class="text-center">Visit 4 </h4>
          <?php $i=0; ?>
          @foreach($entity->visit_images_link4 as $image)
          <div class="single-slider-img gallery-holder">
            <a class="gallery-delete4 btn btn-danger btn-sm color-black" style="padding: 8px 12px; margin-bottom: 5px; text-align: center;">
              <i class="fa fa-trash"></i>
              Delete
              <input class="certificate_delete" type="hidden" value="visit_images_link4+visit_images4+{{$i}}">
            </a>
            <div class="clearfix"></div>
            <div class="certImg">
              <?php
              echo "<a href=".$image." "."data-lightbox=".'"visit_images_link4"'.">";
              echo '<img class="img-responsive" src="'.$image.'" alt="">'."</a>";
              ?>
            </div>
          </div>
          <?php $i++; ?>
          @endforeach
          <div class="clearfix"></div>
          <div class="margin-top-20" >
            <a class="btn bg-white color-black btn-file">
              <i class="fa fa-upload"></i><input type="file" multiple="" name="upload_files[]" accept="image/png,image/jpg,image/jpeg">
              Upload
            </a>
            <button type="submit" class="btn btn-orange color-black btn-md pull-right"><i class="fa fa-save"></i> Save</button>
          </div>
        </form>
        </div>
      </div>
    </form>
  </div>
  <!-- /tile body -->
</section>
</div>
</div>
<!-- /row -->
@stop
@section('custom_script')
<script>
  $(function(){
    $("#theformId").validate();
  });
  $(function(){
// Initialize card flip
$('.card.hover').hover(function(){
  $(this).addClass('flip');
},function(){
  $(this).removeClass('flip');
});
})

</script>
<script>
  $(document).ready(function(){
    $('#division').on('change',function(){
      $division = $(this).val();
      $.ajax({
        method: "GET",
        url: "../division",
        data: { division: $division }
      })
      .success(function( result ) {
        console.log(result);
        $('#district').empty();
        $('#district').append('<option value="">Please choose</option>');
        result.forEach(function(singledata){
          $('#district').append('<option value="'+singledata[0]+'">'+singledata[0]+'</option>')
        });
        $("#district").trigger("chosen:updated");
      });
    });
  });
</script>
<script>
  $(document).ready(function(){
    $('#district').on('change',function(){
      $district = $(this).val();
      $.ajax({
        method: "GET",
        url: "../district",
        data: { district: $district }
      })
      .success(function( result ) {
        console.log(result);
        $('#thana').empty();
        $('#thana').append('<option value="">Please choose</option>');
        result.forEach(function(singledata){
          $('#thana').append('<option value="'+singledata[0]+'">'+singledata[0]+'</option>')
        });
        $("#thana").trigger("chosen:updated");
      });
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#theformId").validate();
  });
</script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script>
// prepare the form when the DOM is ready
$(document).ready(function() {
  var options = {
    url: "../ajax_certificate_link",
    success: function(result){
      console.log(result);
      $('#certificate_link').find('.img-delete').attr('disabled',false);
      $('#certificate_link').find('.certImg img').remove()
      $newdiv = '<a href="'+result+'" data-lightbox="certificate_link">'+ '<img class="img-responsive" src="'+result+'" alt=""></a>';
      $('#certificate_link').find('.certImg').append($newdiv);
    }
  };
  $('#certificate_link').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});
</script>
<script>
// prepare the form when the DOM is ready
$(document).ready(function() {
  var options = {
    url: "../ajax_visit/1",
    success: function(result){
      console.log(result);
      $('#visit_id_1').find('.img-delete').attr('disabled',false);
      $('#visit_id_1').find('.certImg img').remove()
      $newdiv = '<a href="'+result+'" data-lightbox="visit_certificate_link1">'+ '<img class="img-responsive" src="'+result+'" alt=""></a>';
      $('#visit_id_1').find('.certImg').append($newdiv);
    }
  };
  $('#visit_id_1').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});
</script>
<script>
// prepare the form when the DOM is ready
$(document).ready(function() {
  var options = {
    url: "../ajax_visit/2",
    success: function(result){
      console.log(result);
      $('#visit_id_2').find('.img-delete').attr('disabled',false);
      $('#visit_id_2').find('.certImg img').remove()
      $newdiv = '<a href="'+result+'" data-lightbox="visit_certificate_link2">'+ '<img class="img-responsive" src="'+result+'" alt=""></a>';
      $('#visit_id_2').find('.certImg').append($newdiv);
    }
  };
  $('#visit_id_2').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});
</script>
<script>
// prepare the form when the DOM is ready
$(document).ready(function() {
  var options = {
    url: "../ajax_visit/3",
    success: function(result){
      console.log(result);
      $('#visit_id_3').find('.img-delete').attr('disabled',false);
      $('#visit_id_3').find('.certImg img').remove()
      $newdiv = '<a href="'+result+'" data-lightbox="visit_certificate_link3">'+ '<img class="img-responsive" src="'+result+'" alt=""></a>';
      $('#visit_id_3').find('.certImg').append($newdiv);
    }
  };
  $('#visit_id_3').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});
</script>
<script>
// prepare the form when the DOM is ready
$(document).ready(function() {
  var options = {
    url: "../ajax_visit/4",
    success: function(result){
      console.log(result);
      $('#visit_id_4').find('.img-delete').attr('disabled',false);
      $('#visit_id_4').find('.certImg img').remove()
      $newdiv = '<a href="'+result+'" data-lightbox="visit_certificate_link4">'+ '<img class="img-responsive" src="'+result+'" alt=""></a>';
      $('#visit_id_4').find('.certImg').append($newdiv);
    }
  };
  $('#visit_id_4').submit(function() {
    $(this).ajaxSubmit(options);
    return false;
  });
});
</script>
<script>
  jQuery(document).ready(function() {
    var base_url =  {{"'".url('/')."'"}};
    $('.img-delete').on('click',function(e){
      $imginfo = $(this).closest('.img-holder').find('.certificate_delete').val();
      $entityid = $(this).closest('form').find('.entityid').val();
      $this = $(this);
//console.log($entityid);
//ajax code start
$.ajax({
  url: '../certificate_delete',
  type: 'GET',
  data: {id: $entityid, image_name:$imginfo },
})
.done(function(result) {
  console.log("success");
  if(result == 'success')
  {
    $parent = $this.closest('.img-holder').find('.certImg');
    $parent.find('a').remove();
    $newdiv = '<img class="img-responsive" src="'+base_url+'/assets/images/no.jpg'+'" alt="">';
    $parent.append($newdiv);
    $this.attr('disabled',true);
  }
});

//ajax code end
e.preventDefault();
});
  });
</script>
<script>
  jQuery(document).ready(function() {
    var base_url =  {{"'".url('/')."'"}};

    $('.gallery-delete1').on('click',function(e){
      $imginfo = $(this).closest('.gallery-holder').find('.certificate_delete').val();
      $entityid = $(this).closest('form').find('.entityid').val();
      $this = $(this);
//console.log($entityid);
//ajax code start
$.ajax({
  url: '../gallery_delete/1',
  type: 'GET',
  data: {id: $entityid, image_name:$imginfo },
})
.done(function(result) {
  console.log("success");
  if(result == 'success')
  {
    $parent = $this.closest('.gallery-holder');
    $parent.remove();

    $('#gallery_link_1').find('.certificate_delete').each(function(index){
              $(this).val('visit_images_link1+visit_images1+'+index);
          });
  }
});

//ajax code end
e.preventDefault();
});
  });
</script>
<script>
  jQuery(document).ready(function() {
    var base_url =  {{"'".url('/')."'"}};

    $('.gallery-delete2').on('click',function(e){
      $imginfo = $(this).closest('.gallery-holder').find('.certificate_delete').val();
      $entityid = $(this).closest('form').find('.entityid').val();
      $this = $(this);
//console.log($entityid);
//ajax code start
$.ajax({
  url: '../gallery_delete/2',
  type: 'GET',
  data: {id: $entityid, image_name:$imginfo },
})
.done(function(result) {
  console.log("success");
  if(result == 'success')
  {
    $parent = $this.closest('.gallery-holder');
    $parent.remove();

    $('#gallery_link_2').find('.certificate_delete').each(function(index){
              $(this).val('visit_images_link2+visit_images2+'+index);
          });
  }
});

//ajax code end
e.preventDefault();
});
  });
</script>
<script>
  jQuery(document).ready(function() {
    var base_url =  {{"'".url('/')."'"}};

    $('.gallery-delete3').on('click',function(e){
      $imginfo = $(this).closest('.gallery-holder').find('.certificate_delete').val();
      $entityid = $(this).closest('form').find('.entityid').val();
      $this = $(this);
      //console.log($entityid);
      //ajax code start
      $.ajax({
        url: '../gallery_delete/3',
        type: 'GET',
        data: {id: $entityid, image_name:$imginfo },
      })
      .done(function(result) {
        console.log("success");
        if(result == 'success')
        {
          $parent = $this.closest('.gallery-holder');
          $parent.remove();

          $('#gallery_link_3').find('.certificate_delete').each(function(index){
              $(this).val('visit_images_link3+visit_images3+'+index);
          });
        }
      });

      //ajax code end
      e.preventDefault();
      });
  });
</script>
<script>
  jQuery(document).ready(function() {
    var base_url =  {{"'".url('/')."'"}};

    $('.gallery-delete4').on('click',function(e){
      $imginfo = $(this).closest('.gallery-holder').find('.certificate_delete').val();
      $entityid = $(this).closest('form').find('.entityid').val();
      $this = $(this);
      //console.log($entityid);
      //ajax code start
      $.ajax({
        url: '../gallery_delete/4',
        type: 'GET',
        data: {id: $entityid, image_name:$imginfo },
      })
      .done(function(result) {
        console.log("success");
        if(result == 'success')
        {
          $parent = $this.closest('.gallery-holder');
          $parent.remove();

          $('#gallery_link_4').find('.certificate_delete').each(function(index){
              $(this).val('visit_images_link4+visit_images4+'+index);
          });
        }
      });

    //ajax code end
    e.preventDefault();
    });
  });
</script>
@stop