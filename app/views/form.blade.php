@extends('layouts.default.master')
@section('content')

<div class="main">
	<!-- row -->
	<div class="row">
		<!-- col 12 -->
		<div class="col-md-12">
			<!-- tile -->
			<section class="tile color transparent-black">
				<!-- tile header -->
				<div class="tile-header">
					<h1><strong>{{ucfirst($type)}}</strong></h1>
					<div class="controls">
						<a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
						<a href="#" class="remove"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<!-- /tile header -->
				<!-- tile body -->
				<div class="tile-body">
					<form class="form-horizontal" role="form" action="{{URL::to('form_submit')}}" method="post" enctype="multipart/form-data" id="theformId">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="agency" class="col-sm-4 control-label">Agency</label>
								<div class="col-sm-8" id="selectbox">
									<select class="chosen-select chosen-transparent form-control" id="agency" required="true" name="agency">
									
										<option value="">Please choose</option>
										
										@foreach(Agency::lists('name') as $agency)
											<option value="{{$agency}}">{{$agency}}</option>
										@endforeach
									</select>
								</div>
								
							</div>
							<div class="form-group">
								<label for="datepicker" class="col-sm-4 control-label">Date</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepickermulti" id="" name="date" required="true">
								</div>
							</div>
							<div class="form-group">
								<label for="country" class="col-sm-4 control-label">Country</label>
								<div class="col-sm-8">
									<input placeholder="Bangladesh" type="text" class="form-control" id="country" name="country" value="Bangladesh" readonly>
								</div>
							</div>
							<div class="form-group">
								<label for="division" class="col-sm-4 control-label">Division</label>
								<div class="col-sm-8" id="selectbox">
									<select class="chosen-select chosen-transparent form-control" id="division" required="true" name="division">
									
										<option value="">Please choose</option>
										
										@foreach(Area::distinct('division')->get() as $division)
											<option value="{{$division[0]}}">{{$division[0]}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="district" class="col-sm-4 control-label">District</label>
								<div class="col-sm-8" id="selectbox1">
									<select class="chosen-select chosen-transparent form-control" id="district" required="true" name="district">							
										<option value="">Please choose division</option>
										
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="thana" class="col-sm-4 control-label">Thana</label>
								<div class="col-sm-8" id="selectbox2">
									<select class="chosen-select chosen-transparent form-control" id="thana" required="true" name="thana">
										<option value="">Please choose district</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="agency" class="col-sm-4 control-label">Union</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="union" required="true" name="union" pattern="[A-Z][a-z]*">
									
								</div>
							</div>
							<div class="form-group">
								<label for="agency" class="col-sm-4 control-label">Village</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" id="village" required="true" name="village" pattern="[A-Z][a-z]*">
								</div>
							</div>
						</div>

						<div class="col-sm-6">

							{{-- <div id="parentelem">
								<div class="form-group" id="btnfield">
									<div class="col-sm-offset-4 col-sm-8">
										<input type="button" class="btn btn-primary" id="uthan" value="Uthan">
										<input type="button" class="btn btn-success" id="haat" value="Haat">
										<input type="button" class="btn btn-default" id="college" value="College">
									</div>
								</div>
							</div> --}}
							@if($type == 'uthan')
							<input type="hidden" value="uthan" name="type">
							<div id="btn1group">
								<div class="form-group">
									<label for="ownerName" class="col-sm-4 control-label">Uthan Owner's Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="uthan_owner_name" required="true" name="uthan_owner_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" >
										
									</div>
								</div>

								<div class="form-group">
									<label for="phonenum" class="col-sm-4 control-label">Uthan Owner's Cell *</label>
									<div class="col-sm-8">										
										<input type="text" class="form-control" id="uthan_owner_mobile" required="true" placeholder="" name="uthan_owner_mobile" pattern="^01[5-9]\d{8}$">
										
									</div>
								</div>

								<div class="form-group">
									<label for="participantName" class="col-sm-4 control-label">Uthan Participant's Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="uthan_participant_name" required="true" name="uthan_participant_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" >
									</div>
								</div>

								<div class="form-group">
									<label for="phonenum" class="col-sm-4 control-label">Uthan Participant's Cell *</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="uthan_participant_mobile" required="true" name="uthan_participant_mobile" pattern="^01[5-9]\d{8}$">
									</div>
								</div>
							</div>
							@endif
								

							@if($type == 'haat')
							<input type="hidden" value="haat" name="type">
							<div id="btn2group">
								<div class="form-group">
									<label for="ownerName" class="col-sm-4 control-label">Shop Owner's Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="shop_owner_name" required="true" name="shop_owner_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$" >
									</div>
								</div>

								<div class="form-group">
									<label for="phonenum" class="col-sm-4 control-label">Shop Owner/'s Cell *</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="shop_owner_mobile" required="true" placeholder="" name="shop_owner_mobile" pattern="^01[5-9]\d{8}$">
									</div>
								</div>

								<div class="form-group">
									<label for="participantName" class="col-sm-4 control-label">Haat Participant's Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="haat_participant_name" required="true" name="haat_participant_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$">
									</div>
								</div>

								<div class="form-group">
									<label for="phonenum" class="col-sm-4 control-label">Haat Participant\'s Cell *</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="haat_participant_mobile" required="true" placeholder="" name="haat_participant_mobile" pattern="^01[5-9]\d{8}$">
									</div>
								</div>
							</div>
							@endif

							@if($type == 'college')
							<input type="hidden" value="college" name="type">
							<div id="btn3group">
								<div class="form-group">
									<label for="ownerName" class="col-sm-4 control-label">College Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="college_name" required="true" name="college_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$">
									</div>
								</div>
								<div class="form-group">
									<label for="ownerName" class="col-sm-4 control-label">Principal Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="principal_name" required="true" name="principal_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$">
									</div>
								</div>
								<div class="form-group">
									<label for="phonenum" class="col-sm-4 control-label">Principal/'s Cell *</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="principal_mobile" required="true" placeholder="" name="principal_mobile" pattern="^01[5-9]\d{8}$">
									</div>
								</div>
								<div class="form-group">
									<label for="ownerName" class="col-sm-4 control-label">Teacher Name</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="teacher_name" required="true" name="teacher_name" pattern="^([ \u00c0-\u01ffa-zA-Z'\-])+$">										
									</div>
								</div>
								<div class="form-group">
									<label for="phonenum" class="col-sm-4 control-label">Teacher's Cell *</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="teacher_mobile" required="true" placeholder="" name="teacher_mobile" pattern="^01[5-9]\d{8}$">
									</div>
								</div>
							</div>
							@endif

							<div class="form-group">
								<label for="numval" class="col-sm-4 control-label">Total Contact</label>
								<div class="col-sm-8">
									<input type="number" required class="form-control" id="numval" placeholder="must be a number..." pattern="^[1-9]+[0-9]*" name="total_contact" min="1">
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<div class="card-container col-sm-2">
								<div class="card card-redbrown hover">
									<div class="front">
										<div class="media-body">
											<h1 class="media-heading"  style="color:#fff; margin-top:20%;">Visit 1</h1>
										</div>
									</div>
									<div class="back">
										<a href="#">
											<i class="fa fa-bar-chart-o fa-4x"></i>
										</a>
									</div>
								</div>
							</div>

							<div class="col-sm-10">


								<label for="datepicker" class="col-sm-1 control-label">Date</label>
								<div class="col-sm-11">
								<input type="text" class="form-control datepickermulti" id="" name="date1">
								</div>

								<label for="colorpicker-rgb" class="col-sm-1 control-label">Certificate</label>

								<div class="col-sm-11 mtop">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="certificate1" accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>

								<label for="colorpicker-rgb" class="col-sm-1 control-label">Images</label>
								<div class="col-sm-11 mbtm mtop">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="images1[]" multiple accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>
							</div>			            
						</div>
						<div class="form-group">
							<div class="card-container col-sm-2">
								<div class="card card-greensea hover">
									<div class="front">
										<div class="media-body">
											<h1 class="media-heading"  style="color:#fff; margin-top:20%;">Visit 2</h1>
										</div>
									</div>
									<div class="back">
										<a href="#">
											<i class="fa fa-bar-chart-o fa-4x"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-sm-10">
								<label for="datepicker" class="col-sm-1 control-label">Date</label>
								<div class="col-sm-11">
									<input type="text" class="form-control datepickermulti" id="" name="date2">
								</div>						
								<label for="colorpicker-rgb" class="col-sm-1 control-label">Certificate</label>
								<div class="col-sm-11 mbtm mtop">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="certificate2" accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>

								<label for="colorpicker-rgb" class="col-sm-1 control-label">Images</label>
								<div class="col-sm-11">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="images2[]" multiple accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>
							</div>

						</div>
						<div class="form-group">	
							<div class="card-container col-sm-2">
								<div class="card card-dutch hover">
									<div class="front">
										<div class="media-body">
											<h1 class="media-heading"  style="color:#fff; margin-top:20%;">Visit 3</h1>
										</div>
									</div>
									<div class="back">
										<a href="#">
											<i class="fa fa-bar-chart-o fa-4x"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-sm-10">
								<label for="datepicker" class="col-sm-1 control-label">Date</label>
								<div class="col-sm-11">
									<input type="text" class="form-control datepickermulti" id="" name="date3">
								</div>						
								<label for="colorpicker-rgb" class="col-sm-1 control-label">Certificate</label>
								<div class="col-sm-11 mbtm mtop">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="certificate3" accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>
								<label for="colorpicker-rgb" class="col-sm-1 control-label">Images</label>
								<div class="col-sm-11">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="images3[]" multiple accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>							            
							</div>							            
						</div>
						<div class="form-group">	
							<div class="card-container col-sm-2">
								<div class="card card-orange hover">
									<div class="front">
										<div class="media-body">
											<h1 class="media-heading" style="color:#fff; margin-top:20%;">Visit 4</h1>
										</div>
									</div>
									<div class="back">
										<a href="#">
											<i class="fa fa-bar-chart-o fa-4x"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="col-sm-10">
								<label for="datepicker" class="col-sm-1 control-label">Date</label>
								<div class="col-sm-11">
									<input type="text" class="form-control datepickermulti" id="" name="date4">
								</div>						
								<label for="colorpicker-rgb" class="col-sm-1 control-label">Certificate</label>
								<div class="col-sm-11 mbtm mtop">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="certificate4" accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>


								<label for="colorpicker-rgb" class="col-sm-1 control-label">Images</label>
								<div class="col-sm-11 mbtm mtop">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="images4[]" multiple accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>					           
							</div>
								<h4 for="colorpicker-rgb" class="col-sm-3 control-label">Completion Certificate</h4>

								<div class="col-sm-9">
									<div class="input-group">
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-upload"></i><input type="file" name="certificate" accept="image/png,image/jpg,image/jpeg">
											</span>
										</span>
										<input type="text" class="form-control" readonly="">
									</div>
								</div>	
						</div>

						<div class="form-group form-footer">
							<div class="col-sm-offset-4 col-sm-8">
								<button type="submit" class="btn btn-primary">Submit</button>
								<button type="reset" class="btn btn-default">Reset</button>
							</div>
						</div>
					</form>
				</div>
				<!-- /tile body -->
			</section>
			<!-- /tile -->
		</div>
		<!-- /col 6 -->
		<!-- col 12 -->
	</div>
	<!-- /row -->
</div>
@stop

@section('custom_script')

<script>

	$(function(){
		$("#theformId").validate();
	});

	$(function(){

      // Initialize card flip
      $('.card.hover').hover(function(){
      	$(this).addClass('flip');
      },function(){
      	$(this).removeClass('flip');
      });

      
      
  })
	    


      
</script>

<script>
	
$(document).ready(function(){

	$('#division').on('change',function(){

		$division = $(this).val();

		$.ajax({
		  method: "GET",
		  url: "../division",
		  data: { division: $division }
		})
		.success(function( result ) {
			console.log(result);

			$('#district').empty();
			$('#district').append('<option value="">Please choose</option>');
			result.forEach(function(singledata){
				$('#district').append('<option value="'+singledata[0]+'">'+singledata[0]+'</option>')
			});


			$("#district").trigger("chosen:updated");

		});

	});
});

</script>

<script>
	
$(document).ready(function(){

	$('#district').on('change',function(){

		$district = $(this).val();

		$.ajax({
		  method: "GET",
		  url: "../district",
		  data: { district: $district }
		})
		.success(function( result ) {
			console.log(result);

			$('#thana').empty();
			$('#thana').append('<option value="">Please choose</option>');
			result.forEach(function(singledata){
				$('#thana').append('<option value="'+singledata[0]+'">'+singledata[0]+'</option>')
			});


			$("#thana").trigger("chosen:updated");

		});

	});
});

</script>
@stop