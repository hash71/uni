@extends('layouts.default.master')
@section('content')



<!-- tile -->
<section class="tile transparent">


<!-- tile header -->
<div class="tile-header transparent">
<h1><strong>Advanced</strong> Datatable </h1>
<span class="note">including: <span class="italic">TableTools and ColVis</span></span>
<div class="controls">
  <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
  <a href="#" class="remove"><i class="fa fa-times"></i></a>
</div>
</div>
<!-- /tile header -->

<!-- tile body -->
<div class="tile-body color blue rounded-corners">

<div class="table-responsive">

<table class="table table-datatable table-custom" id="reportTable">
  <thead>
    <tr>
      <th>agency</th>
      <th>date</th>
      <th>country</th>
      <th>division</th>
      <th>district</th>
      <th>thana</th>
      <th>union</th>
      <th>village</th>
      <th>uthan_owner_name</th>
      <th>uthan_owner_mobile</th>
      <th>uthan_participant_name</th>
      <th>uthan_participant_mobile</th>
      <th>shop_owner_name</th>
      <th>shop_owner_mobile</th>
      <th>haat_participant_name</td> 
      <th>haat_participant_mobile</th>
      <th>college_name</th>
      <th>principal_name</th>
      <th>principal_mobile</th>
      <th>teacher_name</th>
      <th>teacher_mobile</th>
      <th>total_contact</th>
      <th>visit1</th>
      <th>visit2</th>
      <th>visit3</th> 
      <th>visit4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>                            
      <td></td>                            
      <td></td>                            
      <td></td>                            
      <td></td>
    </tr>
  </tbody>
</table>

</div>

</div>
<!-- /tile body -->



</section>
<!-- /tile -->











@stop



@section('custom_script')
  
  {{HTML::style('//cdn.datatables.net/tabletools/2.2.3/css/dataTables.tableTools.css')}}
  {{HTML::script('//cdn.datatables.net/tabletools/2.2.3/js/dataTables.tableTools.min.js')}}
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
  
  <script src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
  <script src="http://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js" ></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

  <script>

    var oTable = null;

    $(document).ready(function(){ 

      oTable = $('#reportTable').dataTable({
        "scrollX": true,

  // "ajax":'allBlogs',
  "sAjaxSource":'reportdata',
  "bProcessing": true,
  "bServerSide": true, 
  "tableTools": {
            "sSwfPath": "../swf/copy_csv_xls_pdf.swf"
  }, 
  "oTableTools": {
    "sSwfPath": "assets/js/vendor/datatables/tabletools/swf/copy_csv_xls_pdf.swf",
    "aButtons": [
      "copy",
      "print",
      {
        "sExtends":    "collection",
        "sButtonText": 'Save <span class="caret" />',
        "aButtons":    [ "csv", "xls", "pdf" ]
      }
    ]
  },  
  "aoColumns": [
  { "mDataProp" : "agency"},
  { "mDataProp" : "date"},
  { "mDataProp" : "country"},
  { "mDataProp" : "division"},
  { "mDataProp" : "district"},
  { "mDataProp" : "thana"},
  { "mDataProp" : "union"},
  { "mDataProp" : "village"},
  { "mDataProp" : "uthan_owner_name"},
  { "mDataProp" : "uthan_owner_mobile"},
  { "mDataProp" : "uthan_participant_name"},
  { "mDataProp" : "uthan_participant_mobile"},
  { "mDataProp" : "shop_owner_name"},
  { "mDataProp" : "shop_owner_mobile"},
  { "mDataProp" : "haat_participant_name"},
  { "mDataProp" : "haat_participant_mobile"},
  { "mDataProp" : "college_name"},
  { "mDataProp" : "principal_name"},
  { "mDataProp" : "principal_mobile"},
  { "mDataProp" : "teacher_name"},
  { "mDataProp" : "teacher_mobile"},
  { "mDataProp" : "total_contact"},
  { "mDataProp" : "visit1"},
  { "mDataProp" : "visit2"},
  { "mDataProp" : "visit3"},
  { "mDataProp" : "visit4"}
  ],           

  // "lengthMenu": [[5,10,15,-1],[5,10,15,"All"]]   
});
    });

  </script>

  @stop