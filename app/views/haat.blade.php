@extends('layouts.default.master')
@section('content')


          <!-- row -->
<!-- col 12 -->
<div class="col-md-8">
  <!-- tile -->
  <section class="tile color blue">
    <!-- tile header -->
    <div class="tile-header color rounded-top-corners">
      <h1><strong>Uthan Details</strong></h1>
      <div class="controls">
        <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
        <a href="#" class="remove"><i class="fa fa-times"></i></a>
      </div>
    </div>
    <!-- /tile header -->
    <!-- tile body -->
    <div class="tile-body">
      <div class="form-details">
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>Agency</label>
          </div>
          <div class="col-sm-8">
            <p>2015</p>
          </div>
        </div>
        <div class="single-details even">
          <div class="col-sm-4">
            <label>Date</label>
          </div>
          <div class="col-sm-8">
            <p>Searchlite Communications Ltd</p>
          </div>
        </div>
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>Country</label>
          </div>
          <div class="col-sm-8">
            <p>Q1 (JAN-MAR)</p>
          </div>
        </div>
        <div class="single-details even">
          <div class="col-sm-4">
            <label>Division</label>
          </div>
          <div class="col-sm-8">
            <p>UBL_S_3073_8E4P4</p>
          </div>
        </div>
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>District</label>
          </div>
          <div class="col-sm-8">
            <p>Bangladesh</p>
          </div>
        </div>
        <div class="single-details even">
          <div class="col-sm-4">
            <label>Thana</label>
          </div>
          <div class="col-sm-8">
            <p>Dhaka</p>
          </div>
        </div>
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>Union</label>
          </div>
          <div class="col-sm-8">
            <p>Jamalpur</p>
          </div>
        </div>
        <div class="single-details even">
          <div class="col-sm-4">
            <label>Village</label>
          </div>
          <div class="col-sm-8">
            <p>Dewanganj</p>
          </div>
        </div>
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>Shop Owner's Name</label>
          </div>
          <div class="col-sm-8">
            <p>DEWANGANJ</p>
          </div>
        </div>
        <div class="single-details even">
          <div class="col-sm-4">
            <label>Shop Owner's Cell </label>
          </div>
          <div class="col-sm-8">
            <p>56 NO JHAWDANGGA GOVERNMENT PRIMARY SCHOOL</p>
          </div>
        </div>
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>Uthan Participant's Name</label>
          </div>
          <div class="col-sm-8">
            <p>Mrs. ANJUMAN ARA</p>
          </div>
        </div>
        <div class="single-details even">
          <div class="col-sm-4">
            <label>Uthan Participant's Cell</label>
          </div>
          <div class="col-sm-8">
            <p></p>
          </div>
        </div>
        <div class="single-details odd">
          <div class="col-sm-4">
            <label>Total Contact</label>
          </div>
          <div class="col-sm-8">
            <p>+8801846169239</p>
          </div>
        </div>
      </div>
    </div>
    <!-- /tile body -->
  </section>
  <!-- /tile -->
</div>
<!-- /col 9 -->
<div class="col-md-4">
  <section class="tile color" style="background-color:#F64B1E">
    <!-- tile header -->
    <div class="tile-header text-center">
      <h1><strong>Completion Certificate</strong></h1>
      <hr>
    </div>
    <!-- /tile header -->
    <!-- tile body -->
    <div class="tile-body">
      <div class="certImg">
        <a href="">
          <img class="img-responsive" src="assets/images/certificate-thumb/1.jpg" alt="">
        </a>
      </div>
    </div>
    <!-- /tile body -->
  </section>
  <section class="tile color" style="background-color:#731627">
    <!-- tile widget -->
    <div class="tile-widget nopadding color transparent-black rounded-top-corners">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#visit1" aria-controls="visit1" role="tab" data-toggle="tab">Visit  1</a></li>
        <li role="presentation"><a href="#visit2" aria-controls="visit2" role="tab" data-toggle="tab">Visit  2</a></li>
        <li role="presentation"><a href="#visit3" aria-controls="visit3" role="tab" data-toggle="tab">Visit  3</a></li>
        <li role="presentation"><a href="#visit4" aria-controls="visit4" role="tab" data-toggle="tab">Visit  4</a></li>
      </ul>
      <!-- / Nav tabs -->
    </div>
    <!-- /tile widget -->
    <!-- tile body -->
    <div class="tile-body rounded-bottom-corners">
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="visit1">
          <h2 class="text-center"><strong>Visit Details</strong></h2>
          <h5 class="text-center"><strong>Visit Date</strong> Jan 12, 2015</h5>
          <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
          <div class="certImg">
            <a href="">
              <img class="img-responsive" src="assets/images/certificate-thumb/1.jpg" alt="">
            </a>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit2">
          <h2 class="text-center"><strong>Visit Details</strong></h2>
          <h5 class="text-center"><strong>Visit Date</strong> Feb 03, 2015</h5>
          <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
          <div class="certImg">
            <a href="">
              <img class="img-responsive" src="assets/images/certificate-thumb/1.jpg" alt="">
            </a>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit3">
          <h2 class="text-center"><strong>Visit Details</strong></h2>
          <h5 class="text-center"><strong>Visit Date</strong> Feb 24, 2015</h5>
          <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
          <div class="certImg">
            <a href="">
              <img class="img-responsive" src="assets/images/certificate-thumb/1.jpg" alt="">
            </a>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit4">
          <h2 class="text-center"><strong>Visit Details</strong></h2>
          <h5 class="text-center"><strong>Visit Date</strong> Mar 30, 2015</h5>
          <p class="text-center">ACKNOWLEDGEMENT CERTIFICATES</p>
          <div class="certImg">
            <a href="">
              <img class="img-responsive" src="assets/images/certificate-thumb/1.jpg" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- /tile body -->
  </section>
  <section class="tile color" style="background-color:#A63F82">
    <!-- tile widget -->
    <div class="tile-widget nopadding color transparent-black rounded-top-corners">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#visit1Photos" aria-controls="visit1Photos" role="tab" data-toggle="tab">Visit 1</a></li>
        <li role="presentation"><a href="#visit2Photos" aria-controls="visit2Photos" role="tab" data-toggle="tab">Visit 2</a></li>
        <li role="presentation"><a href="#visit3Photos" aria-controls="visit3Photos" role="tab" data-toggle="tab">Visit 3</a></li>
        <li role="presentation"><a href="#visit4Photos" aria-controls="visit4Photos" role="tab" data-toggle="tab">Visit 4</a></li>
      </ul>
      <!-- / Nav tabs -->
    </div>
    <!-- /tile widget -->
    <!-- tile body -->
    <div class="tile-body rounded-bottom-corners">
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="visit1Photos">
          <h2 class="text-center"><strong>Visit Photos</strong></h2>
          <h4 class="text-center">Visit 1 </h4>
          <div id="owl-example" class="owl-carousel">
            <div class="text-center">
              <a href=""><img src="assets/images/school-slides/1.jpg" alt="" ></a>
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/2.jpg" alt="" >
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/3.jpg" alt="">
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit2Photos">
          <h2 class="text-center"><strong>Visit Photos</strong></h2>
          <h4 class="text-center">Visit 2 </h4>
          <div id="owl-example" class="owl-carousel">
            <div class="text-center">
              <a href=""><img src="assets/images/school-slides/1.jpg" alt="" ></a>
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/2.jpg" alt="" >
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/3.jpg" alt="">
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit3Photos">
          <h2 class="text-center"><strong>Visit Photos</strong></h2>
          <h4 class="text-center">Visit 3 </h4>
          <div id="owl-example" class="owl-carousel">
            <div class="text-center">
              <a href=""><img src="assets/images/school-slides/1.jpg" alt="" ></a>
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/2.jpg" alt="" >
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/3.jpg" alt="">
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="visit4Photos">
          <h2 class="text-center"><strong>Visit Photos</strong></h2>
          <h4 class="text-center">Visit 4 </h4>
          <div id="owl-example" class="owl-carousel">
            <div class="text-center">
              <a href=""><img src="assets/images/school-slides/1.jpg" alt="" ></a>
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/2.jpg" alt="" >
            </div>
            
            <div class="text-center">
              <img src="assets/images/school-slides/3.jpg" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tile body -->
  </section>
</div>
          <!-- /row -->   






@stop



@section('custom_script')

<script type="text/javascript">
  //owl carousel
  $(".owl-carousel").owlCarousel({
    singleItem: true,
    autoPlay: true,
    navigation: true,
    slideSpeed: 400,
    paginationSpeed: 500,
    navigationText: ['<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>']
  });
</script>
@stop