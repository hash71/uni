<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EntryTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$url = "http://localhost/uni/public";



		foreach(range(1, 10) as $index){

			$cnumber = '017'.$faker->randomNumber($nbDigits = 8);

			if(strlen($cnumber)<11){
				while(1){
					if(strlen($cnumber) == 11)break;
					$cnumber .= "1";
				}
			}	
	
			Entry::create([
				'id'=>time(),
				'type'=>'uthan',
				'agency' => $faker->company,
				'date' =>$faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
				'country'=>'Bangladesh',
				'division'=>'Dhaka',
				'district'=>'Dhaka',
				'thana'=>'dhana',
				'union'=>'dhunion',
				'village'=>'dhillage',
				
				'uthan_owner_name'=>$faker->name(),
				'uthan_owner_mobile'=>$cnumber,
				'uthan_participant_name'=>$faker->name(),
				'uthan_participant_mobile'=>$cnumber-1,

				'shop_owner_name'=>'',
				'shop_owner_mobile'=>'',
				'haat_participant_name'=>'',
				'haat_participant_mobile'=>'',
				

				'college_name'=>'',
				'principal_name'=>'',
				'principal_mobile'=>'',
				'teacher_name'=>'',
				'teacher_mobile'=>'',	

				'total_contact'=>$faker->randomNumber($nbDigits = NULL),

				'visit_images1'=> [
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472563carousel1.jpg'. '>'. 'image'.'2'  . '</a>'
				],
				'visit_images_link1'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472563carousel1.jpg'
				],
                'visit_certificate1'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image' . '</a>',
				'visit_certificate_link1'=>$url.'/uploads/'.'1426472562carousel2.jpg',
                'visit_date1'=>date("Y-m-d h:i:s"),                              
                
                'visit_images2'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426473100carousel4.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426473100carousel3.jpg'. '>'. 'image'.'2' . '</a>'
				],
				'visit_images_link2'=> [
					$url.'/uploads/'.'1426473100carousel4.jpg',
					$url.'/uploads/'.'1426473100carousel3.jpg'
				],
                'visit_certificate2'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426473413carousel1.jpg'. '>'. 'image'. '</a>',
				'visit_certificate_link2'=>$url.'/uploads/'.'1426473413carousel1.jpg',				
                'visit_date2'=>date("Y-m-d h:i:s"),
                'visit_images3'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472563carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426628547carousel2.jpg'. '>'. 'image'.'2'  . '</a>'
				],
				'visit_images_link3'=> [
					$url.'/uploads/'.'1426472563carousel1.jpg',
					$url.'/uploads/'.'1426628547carousel2.jpg'
				],
                'visit_certificate3'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426600711carousel1.jpg'. '>'. 'image'. '</a>',
				'visit_certificate_link3'=>$url.'/uploads/'.'1426624330carousel1.jpg',
                'visit_date3'=>date("Y-m-d h:i:s"),
                'visit_images4'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426628547carousel2.jpg'. '>'. 'image'.'1'. '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426625190carousel1.jpg'. '>'. 'image'.'2' . '</a>'
				],
				'visit_images_link4'=> [
					$url.'/uploads/'.'1426628547carousel2.jpg',
					$url.'/uploads/'.'1426625190carousel1.jpg'
				],
                'visit_certificate4'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'</a>',
				'visit_certificate_link4'=>$url.'/uploads/'.'1426472562carousel2.jpg',				
                'visit_date4'=>date("Y-m-d h:i:s"),
				'certificate'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>', 
				'certificate_link'=>$url.'/uploads/'.'1426472562carousel2.jpg'
				

			]);
		}

		foreach(range(1, 10) as $index){

			$cnumber = '017'.$faker->randomNumber($nbDigits = 8);

			if(strlen($cnumber)<11){
				while(1){
					if(strlen($cnumber) == 11)break;
					$cnumber .= "1";
				}
			}
			
			Entry::create([
				'id'=>time(),
				'type'=>'haat',
				'agency' => $faker->company,
				'date' =>$faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
				'country'=>'Bangladesh',
				'division'=>'Dhaka',
				'district'=>'Dhaka',
				'thana'=>'dhana',
				'union'=>'dhunion',
				'village'=>'dhillage',

				'uthan_owner_name'=>'',
				'uthan_owner_mobile'=>'',
				'uthan_participant_name'=>'',
				'uthan_participant_mobile'=>'',

				'shop_owner_name'=>$faker->name(),
				'shop_owner_mobile'=>$cnumber,
				'haat_participant_name'=>$faker->name(),
				'haat_participant_mobile'=>$cnumber,
				

				'college_name'=>'',
				'principal_name'=>'',
				'principal_mobile'=>'',
				'teacher_name'=>'',
				'teacher_mobile'=>'',				
				
				
				'total_contact'=>$faker->randomNumber($nbDigits = NULL),
				
								'visit_images1'=> [
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2'  . '</a>'
				],
				'visit_images_link1'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate1'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image' . '</a>',
				'visit_certificate_link1'=>$url.'/uploads/'.'1426472562carousel2.jpg',
                'visit_date1'=>date("Y-m-d h:i:s"),                              
                
                'visit_images2'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2' . '</a>'
				],
				'visit_images_link2'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate2'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>',
				'visit_certificate_link2'=>$url.'/uploads/'.'1426472562carousel2.jpg',				
                'visit_date2'=>date("Y-m-d h:i:s"),
                'visit_images3'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2'  . '</a>'
				],
				'visit_images_link3'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate3'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>',
				'visit_certificate_link3'=>$url.'/uploads/'.'1426472562carousel2.jpg',
                'visit_date3'=>date("Y-m-d h:i:s"),
                'visit_images4'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'. '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2' . '</a>'
				],
				'visit_images_link4'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate4'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'</a>',
				'visit_certificate_link4'=>$url.'/uploads/'.'1426472562carousel2.jpg',				
                'visit_date4'=>date("Y-m-d h:i:s"),
				'certificate'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>', 
				'certificate_link'=>$url.'/uploads/'.'1426472562carousel2.jpg'
				

			]);
		}

		foreach(range(1, 10) as $index){

			$cnumber = '017'.$faker->randomNumber($nbDigits = 8);

			if(strlen($cnumber)<11){
				while(1){
					if(strlen($cnumber) == 11)break;
					$cnumber .= "1";
				}
			}
			
			Entry::create([
				
				'id'=>time(),
				'type'=>'college',
				'agency' => $faker->company,
				'date' =>$faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
				'country'=>'Bangladesh',
				'division'=>'Dhaka',
				'district'=>'Dhaka',
				'thana'=>'dhana',
				'union'=>'dhunion',
				'village'=>'dhillage',

				'uthan_owner_name'=>'',
				'uthan_owner_mobile'=>'',
				'uthan_participant_name'=>'',
				'uthan_participant_mobile'=>'',

				'shop_owner_name'=>'',
				'shop_owner_mobile'=>'',
				'haat_participant_name'=>'',
				'haat_participant_mobile'=>'',
				
				
				'college_name'=>$faker->name(),
				'principal_name'=>$faker->name(),
				'principal_mobile'=>$cnumber,
				'teacher_name'=>$faker->name(),
				'teacher_mobile'=>$cnumber,



				'total_contact'=>$faker->randomNumber($nbDigits = NULL),
				
				'visit_images1'=> [
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2'  . '</a>'
				],
				'visit_images_link1'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate1'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image' . '</a>',
				'visit_certificate_link1'=>$url.'/uploads/'.'1426472562carousel2.jpg',
                'visit_date1'=>date("Y-m-d h:i:s"),                              
                
                'visit_images2'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2' . '</a>'
				],
				'visit_images_link2'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate2'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>',
				'visit_certificate_link2'=>$url.'/uploads/'.'1426472562carousel2.jpg',				
                'visit_date2'=>date("Y-m-d h:i:s"),
                'visit_images3'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'  . '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2'  . '</a>'
				],
				'visit_images_link3'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate3'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>',
				'visit_certificate_link3'=>$url.'/uploads/'.'1426472562carousel2.jpg',
                'visit_date3'=>date("Y-m-d h:i:s"),
                'visit_images4'=>[
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel1.jpg'. '>'. 'image'.'1'. '</a>',
					'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'2' . '</a>'
				],
				'visit_images_link4'=> [
					$url.'/uploads/'.'1426472562carousel1.jpg',
					$url.'/uploads/'.'1426472562carousel2.jpg'
				],
                'visit_certificate4'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'.'</a>',
				'visit_certificate_link4'=>$url.'/uploads/'.'1426472562carousel2.jpg',				
                'visit_date4'=>date("Y-m-d h:i:s"),
				'certificate'=>'<a data-lightbox="ab'.'1'.'"'. 'href ='.'/uploads/'.'1426472562carousel2.jpg'. '>'. 'image'. '</a>', 
				'certificate_link'=>$url.'/uploads/'.'1426472562carousel2.jpg'
				

			]);
		}


	}

}