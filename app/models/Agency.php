<?php

class Agency extends MEloquent {
	
	protected $collection = 'agencies';

	protected $guarded = [];

	protected $primary_key = '_id';

	public $timestamps = false;


}