<?php

Route::get('fuk',function(){
    
    $str = 'http://localhost/uni/public/uploads/1427100930aOb0PD6_700b.jpg';
    File::delete(substr($str, strpos($str, 'uploads'), (strlen($str)-strpos($str, 'uploads'))));
    

});

Route::get('mao',function(){
    return View::make('mao');
});


Route::get('login', function(){
    return Redirect::to('/');
});

Route::get('1',function(){

    return Request::root();
    return dd(public_path());
    // return dd(url('/'));
    return dd(base_url());
});

Route::post('editForm/{id}',function($id){

        // return dd(Input::all());

        

        $entry = Entry::find($id);

        $entry->id = time();

        

        

        $entry->agency = Input::get('agency');
        $entry->date = Input::get('date');
        $entry->country = Input::get('country');
        $entry->division = Input::get('division');
        $entry->district = Input::get('district');
        $entry->thana = Input::get('thana');
        $entry->union = Input::get('union');
        $entry->village = Input::get('village');

        $entry->uthan_owner_name = Input::get('uthan_owner_name')?Input::get('uthan_owner_name'):'';
        $entry->uthan_owner_mobile = Input::get('uthan_owner_mobile')?Input::get('uthan_owner_mobile'):'';
        $entry->uthan_participant_name = Input::get('uthan_participant_name')?Input::get('uthan_participant_name'):'';
        $entry->uthan_participant_mobile = Input::get('uthan_participant_mobile')?Input::get('uthan_participant_mobile'):'';

        $entry->shop_owner_name = Input::get('shop_owner_name')?Input::get('shop_owner_name'):'';
        $entry->shop_owner_mobile = Input::get('shop_owner_mobile')?Input::get('shop_owner_mobile'):'';
        $entry->haat_participant_name = Input::get('haat_participant_name')?Input::get('haat_participant_name'):'';
        $entry->haat_participant_mobile = Input::get('haat_participant_mobile')?Input::get('haat_participant_mobile'):'';

        $entry->college_name = Input::get('college_name')?Input::get('college_name'):'';
        $entry->principal_name = Input::get('principal_name')?Input::get('principal_name'):'';
        $entry->principal_mobile = Input::get('principal_mobile')?Input::get('principal_mobile'):'';
        $entry->teacher_name = Input::get('teacher_name')?Input::get('teacher_name'):'';
        $entry->teacher_mobile = Input::get('teacher_mobile')?Input::get('teacher_mobile'):'';

        $entry->total_contact = Input::get('total_contact');
        
        $entry->save();

        return Redirect::to('entity/'.$id);
        
})->before('auth');


Route::post('approval',function(){
    
    
    $entry = Entry::find(Input::get('__id'));

    if(Auth::user()->role == 'agency_admin'){
        $entry->uni_admin = 1;
        $entry->save();
    }
    if(Auth::user()->role == 'uni_admin'){
        $entry->uni_management = 1;
        $entry->save();
    }


    return Redirect::to('dataTable');
})->before('auth');


Route::get('create_agency',function(){
    $agency = new Agency;
    $agency->name = "New Agency";
    $agency->save();

    $agency = new Agency;
    $agency->name = "Old Agency";
    $agency->save();
})->before('auth');

Route::get('p', function(){
    // return 
    return dd(url('/'));
})->before('auth');

Route::post('multi_image_upload/{visit}',function($visit){    
    // return "hello";
    $a = array();

    $b = array();

    $name_array = array();

    $entry = Entry::find(Input::get('__id'));

    $visit_images_link = "visit_images_link".$visit;

    $links = $entry->$visit_images_link;

    $ts = time()+30;


    foreach ($links as $link) {

        $filename = explode('/', $link);

        $new_name = $filename[sizeof($filename)-2].$filename[sizeof($filename)-1];

        $link = '<a data-lightbox="'.$ts.'"'. 'href ='.url('/').'/uploads/'.$new_name. '>'. 'image'. '</a>' ;

        array_push($a, $link);
    }

    // return dd($links);



    

    

    foreach (Input::file('upload_files') as $image) {        

        // $index++;

        $file = $image;

        if($file) {

            $destinationPath = base_path().'/public/uploads/';
            $filename = time().$file->getClientOriginalName();
            $upload_success = $file->move($destinationPath, $filename);

            if ($upload_success) {
                // resizing an uploaded file

                Image::make($destinationPath . $filename)->resize(800, 600)->save($destinationPath . $filename);

                    // return Response::json('success', 200);
            } else {
                    // return Response::json('error', 400);
            }

            $link = '<a data-lightbox="'.$ts.'"'. 'href ='.url('/').'/uploads/'.$filename. '>'. 'image'. '</a>' ;
            $link_path = url('/').'/uploads/'.$filename;
            array_push($a, $link);
            array_push($links, $link_path);
        }


    }

    // return dd($links);

    $visit_bal = 'visit_images'.$visit;
    $visit_link_bal = 'visit_images_link'.$visit;
    $entry->$visit_bal = $a;
    $entry->$visit_link_bal = $links;

    $entry->save();

    return Redirect::to('entity/'.Input::get('__id'));
})->before('auth');


Route::get('certificate_delete', function(){
    // return dd(Input::all());
    $entry = Entry::find(Input::get('id'));


    $fields = explode('+', Input::get('image_name'));

    
    $field0 = $fields[0];
    $field1 = $fields[1];

    $str = $entry->$field0;

    File::delete(substr($str, strpos($str, 'uploads'), (strlen($str)-strpos($str, 'uploads'))));

    // File::delete($entry->$field0); 

    $entry->$field0 = "";
    $entry->$field1 = "";

    $entry->save();

    return Response::json('success');
})->before('auth');


Route::get('gallery_delete/{index}', function($index){
    // return dd(Input::all());
    $entry = Entry::find(Input::get('id'));


    $fields = explode('+', Input::get('image_name'));

    
    $field0 = $fields[0];
    $field1 = $fields[1];
    $field2 = $fields[2];

    $image_array_link =  $entry->$field0;
    $image_array = $entry->$field1;

    
    $link = $image_array_link[$field2];

    $str = $link;

    File::delete(substr($str, strpos($str, 'uploads'), (strlen($str)-strpos($str, 'uploads'))));

    // File::delete($link); 



    unset($image_array_link[$field2]);
    unset($image_array[$field2]);


    // return "hello";
    // return $index;
    $visit_images = "visit_images".$index;
    $visit_images_link = "visit_images_link".$index;

    $entry->$visit_images_link = array_values($image_array_link) ;
    $entry->$visit_images = array_values($image_array);

    $entry->save();

    // return $image_array_link;

    return Response::json('success');
})->before('auth');

Route::get('entity/{id}', function($id){
    if(Auth::user()->role == 'uni_management')
            return Redirect::to('dataTable');

    $entity = Entry::where('_id',$id)->get();

    $entity = $entity[0];

    return View::make('instance',compact('entity'));

})->before('auth');

Route::post('ajax_certificate_link', function(){
    // return "hello";
    //completion certificate
    $file = Input::file('image_file');
    $entry = Entry::find(Input::get('__id'));

    $str = $entry->certificate_link;
    File::delete(substr($str, strpos($str, 'uploads'), (strlen($str)-strpos($str, 'uploads'))));

    if($file) {
        $destinationPath = public_path() . '/uploads/';
        $filename = time().$file->getClientOriginalName();
        $upload_success = $file->move($destinationPath, $filename);
        if ($upload_success) {
    // resizing an uploaded file
            Image::make($destinationPath . $filename)->resize(800, 600)->save($destinationPath . $filename);
    // return Response::json('success', 200);
        } else {
    // return Response::json('error', 400);
        }
        $ts = (time()+20);
        $link = '<a data-lightbox="'.$ts.'"'. 'href ='.url('/').'/uploads/'.$filename. '>'. 'image'.'</a>' ;
    // Entry::update(['certificate'=>$link,'certificate_link'=>public_path().'/uploads/'.$filename])->where('_id',Input::get('__id'));
        $entry->certificate = $link;
        $entry->certificate_link = url('/').'/uploads/'.$filename;
        $entry->save();
        return url('/').'/uploads/'.$filename;
    }
    //completion certificate end

})->before('auth');


Route::post('ajax_visit/{index}', function($index){   

    $file = Input::file('image_file');

    $entry = Entry::find(Input::get('__id'));

    $link_path = null;
    
    if($file) {

        $destinationPath = public_path() . '/uploads/';
        $filename = time().$file->getClientOriginalName();
        $upload_success = $file->move($destinationPath, $filename);
        if ($upload_success) {

            Image::make($destinationPath . $filename)->resize(800, 600)->save($destinationPath . $filename);

        } else {

        }
        $ts = (time()+10).$index;

        $link = '<a data-lightbox="'.$ts.'"'. 'href ='.url('/').'/uploads/'.$filename. '>'. 'image'.'</a>' ;

        $link_path = url('/').'/uploads/'.$filename;

        $visit = 'visit_certificate'.$index;

        $visit_link = 'visit_certificate_link'.$index;

        $entry->$visit = $link;
        $entry->$visit_link = $link_path;
        
    }
    $visit_date = "visit_date".$index;
    $entry->$visit_date = Input::get('date');
    $entry->save();
    return $link_path;
})->before('auth');



Route::get('showentity/{id}',function($id){

    $entity_data = Entry::where('_id',$id)->get();    

    $entity = $entity_data[0];
    
    return View::make('single',compact('entity'));
    
})->before('auth');



Route::get('haat',function(){

    return View::make('haat');
    
})->before('auth');
Route::get('college',function(){

    return View::make('college');
    
})->before('auth');

Route::get('test',function(){

    User::truncate();

    $user = new User;

    $user->username = "agency_admin_vim";
    $user->password = Hash::make("agency_admin");    
    $user->role = "agency_admin";
    $user->status = 2;
    $user->campaign = 'vim';
    $user->save();

    $user = new User;

    $user->username = "operator_vim";
    $user->password = Hash::make("operator");    
    $user->role = "operator";
    $user->status = 1;
    $user->campaign = 'vim';
    $user->save();

    $user = new User;

    $user->username = "uni_admin_vim";
    $user->password = Hash::make("uni_admin");    
    $user->role = "uni_admin";
    $user->status = 3;
    $user->campaign = 'vim';
    $user->save();

    $user = new User;
    
    $user->username = "uni_management_vim";
    $user->password = Hash::make("uni_management");    
    $user->role = "uni_management";
    $user->status = 4;
    $user->campaign = 'vim';
    $user->save();


    $user = new User;

    $user->username = "agency_admin_rin";
    $user->password = Hash::make("agency_admin");    
    $user->role = "agency_admin";
    $user->status = 2;
    $user->campaign = 'rin';
    $user->save();

    $user = new User;

    $user->username = "operator_rin";
    $user->password = Hash::make("operator");    
    $user->role = "operator";
    $user->status = 1;
    $user->campaign = 'rin';
    $user->save();

    $user = new User;

    $user->username = "uni_admin_rin";
    $user->password = Hash::make("uni_admin");    
    $user->role = "uni_admin";
    $user->status = 3;
    $user->campaign = 'rin';
    $user->save();

    $user = new User;
    
    $user->username = "uni_management_rin";
    $user->password = Hash::make("uni_management");    
    $user->role = "uni_management";
    $user->status = 4;
    $user->campaign = 'rin';
    $user->save();


    $user = new User;

    $user->username = "agency_admin_lux";
    $user->password = Hash::make("agency_admin");    
    $user->role = "agency_admin";
    $user->status = 2;
    $user->campaign = 'lux';
    $user->save();

    $user = new User;

    $user->username = "operator_lux";
    $user->password = Hash::make("operator");    
    $user->role = "operator";
    $user->status = 1;
    $user->campaign = 'lux';
    $user->save();

    $user = new User;

    $user->username = "uni_admin_lux";
    $user->password = Hash::make("uni_admin");    
    $user->role = "uni_admin";
    $user->status = 3;
    $user->campaign = 'lux';
    $user->save();

    $user = new User;
    
    $user->username = "uni_management_lux";
    $user->password = Hash::make("uni_management");    
    $user->role = "uni_management";
    $user->status = 4;
    $user->campaign = 'lux';
    $user->save();




});

Route::get('/', function(){
    
    return Redirect::to('users/login');
       
});

Route::get('dashboard',function(){
    $page = 'dashboard';
    return View::make('dashboard',compact('page'));    

})->before('auth');

Route::get('dataTable', function(){
    return View::make('dataTable');
})->before('auth');

Route::get('division',function(){

    $districts = Area::where('division',Input::get('division'))->distinct('district')->get();   

    return Response::json($districts);
})->before('auth');

Route::get('district',function(){

    $thanas = Area::where('district',Input::get('district'))->distinct('thana')->get();   

    return Response::json($thanas);
})->before('auth');




Route::controller('users','UsersController');

Route::get('logout',function(){

    Auth::logout();

    return Redirect::to('users/login');
})->before('auth');

Route::get('dtable', function () {
    return View::make('dtable');
})->before('auth');

Route::get('form/{type}',function($type){
    return View::make('form',compact('type'));
})->before('auth');

Route::get('tablet',function(){
    // return "hello";
    return View::make('tablet');
})->before('auth');
Route::get('table',function(){
    // return "hello";
    return View::make('table');
})->before('auth');
Route::get('reportdata', 'ReportsController@ajax')->before('auth');

Route::post('form_submit','EntriesController@store')->before('auth');

// upload file
Route::get('hello',function(){
    return View::make('hello');
})->before('auth');

Route::post('/upload', function () {
    // return Input::get('name');
    $file = Input::file('file');
    if($file) {
        $destinationPath = public_path() . '/uploads/';
        $filename = $file->getClientOriginalName();
        $upload_success = Input::file('file')->move($destinationPath, $filename);
        
        if ($upload_success) {
            // resizing an uploaded file
            Image::make($destinationPath . $filename)->resize(100, 100)->save($destinationPath . "100x100_" . $filename);
            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }
})->before('auth');
// delete image
Route::post('delete-image', function () {
    $destinationPath = public_path() . '/uploads/';
    File::delete($destinationPath . Input::get('file'));
    File::delete($destinationPath . "100x100_" . Input::get('file'));
    return Response::json('success', 200);
})->before('auth');








